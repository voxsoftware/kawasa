
var Kawasa= core.org.voxsoftware.Database.Kawasa
import Path from 'path'
class Query{

  constructor(){
    //this.funcs=[]
    this.clear()
  }

  clear(){
    this.funcs=[]
    this.values=[]
    this.q= null
  }


  static analizeParts(parts){
    var p=[]
    for(var i=0;i<parts.length;i++){
      p.push({
        text:parts[i],
        number: /^\d+$/.test(parts[i])
      })
    }
    return p

  }

  analizeParts(a){
    return Query.analizeParts(a)
  }

  static gcompare(a, parts, b, indexStart=0){
    var part
    for(var i=indexStart;i<parts.length;i++){
      part= parts[i]
      a= a[part.text]
      if(a  && i<(parts.length-1) && !parts[i+1].number & (a instanceof Array)){
        // Verificar cada elemento del array
        for(var c of a){
          if(Query.gcompare(c, parts, b, i+1))
            return true
        }
        return false
      }
      if(!a)
        break
    }
    return Query.compare(a, b)
  }



  static ucompare(mode, a, parts, b, indexStart=0){
    var part, i, slice

    vw.log(parts)
    for(var i=indexStart;i<parts.length-1;i++){
      part= parts[i]
      a= a[part.text]||{}
      if(a  && i<(parts.length-1) && !parts[i+1].number & (a instanceof Array)){
        // Verificar cada elemento del array
        for(var c of a){
          Query.ucompare(mode, c, parts, b, i+1)
        }
        return
      }
      if(!a)
        break
    }

    if(mode=="$set")
      a[parts[parts.length-1].text]=b
    else if(mode=="$unset")
      delete a[parts[parts.length-1].text]
    else if(mode=="$inc")
      a[parts[parts.length-1].text]=a[parts[parts.length-1].text] +b
    else if(mode=="$max")
      a[parts[parts.length-1].text]= Math.max(a[parts[parts.length-1].text],b)
    else if(mode=="$min")
      a[parts[parts.length-1].text]= Math.min(a[parts[parts.length-1].text],b)


    else if(mode=="$push"){
      // Verificar que sea array
      a= a[parts[parts.length-1].text]
      if(a instanceof Array){
        if(typeof b=="object" && b.$slice){
          slice= b.$slice
          if(!b.$each){
            throw new core.System.Exception("Debe especificar la propiedad each junto a slice")
          }
          b.$each= b.$each.slice(0,Math.min(0, slice-a.length))
        }

        if(typeof b=="object" && b.$each){
          for(var z=0;z<b.$each.lenght;z++){
            a.push(b.$each[z])
          }
        }
        else{
          a.push(b)
        }

        if(a.length> slice){
          a= a.slice(0, slice)
        }
      }

    }

    else if(mode=="$addToSet"){
      // Verificar que sea array
      a= a[parts[parts.length-1].text]
      if(a instanceof Array && a.indexOf(b)<0)
        a.push(b)
    }

    else if(mode=="$pop"){
      // Verificar que sea array
      a= a[parts[parts.length-1].text]
      if(a instanceof Array){
        if(b>0)
          a.pop()
        else if(b<0)
          a.shift()
      }
    }

    else if(mode=="$pull"){
      // Borrar las coincidencias
      a= a[parts[parts.length-1].text]
      if(a instanceof Array){

        if(typeof b=="object" && b.$in){
          for(var z=0;z<b.$in.lenght;z++){
            while(i=a.indexOf(b[z])>=0){
              for(i;i<a.length;i++){
                a[i]= a[i+1]
              }
              a.pop()
            }
          }
        }
        else{
          while(i=a.indexOf(b)>=0){
            for(i;i<a.length;i++){
              a[i]= a[i+1]
            }
            a.pop()
          }
        }
      }
    }






  }


  static copy(b){
    var value, a={}
    for(var id in b){
      value= b[id]
      if(typeof value == "object"){
        a[id]= {}
        Query.copy(a[id], value)
      }
      else{
        a[id]= value
      }
    }
    return a
  }


  static update(a, b){
    var id, parts, o
    //vw.warning("OBJ1:",a)
    if(Object.keys(b.obj).length>0){
      id= a._id
      a= Query.copy(b.obj)
      a._id=id
    }
    //vw.warning("OBJ2:",a, b)

    for(var id2 in b){
      if(id2.startsWith("$")){
        o=b[id2]
        for(var id in o){
          parts= Query.analizeParts(id.split("."))
          Query.ucompare(id2, a, parts, o[id])
        }
      }
    }
    //vw.warning("OBJ3:",a)
    return a
  }




  static processForUpdate(obj){
    var o={}, set, a={}
    for(var id in obj){
      if(id.startsWith("$"))
        a[id]= obj[id]

      else
        o[id]= obj[id]
    }
    a.obj= o
    return a
  }




  static createProjectionMap(proj){
    return (a)=>{
      Query.projection(a, proj.projection, proj.mode)
      return a
    }
  }

  static projection(value, projection, mode, prefix=''){
    var val, elim, l
    for(var id in value){

      val= value[id]
      elim= false
      n= prefix+(prefix?".":"")+id//Path.join(prefix, id)
      //vw.error(n)
      if(mode==0){
        // Se eliminan solo algunso
        l= projection[n]
        if(l==0){
          delete value[id]
          elim= true
        }
      }
      else if(mode==1){

        l= projection[n]
        if((n=="_id" && l==0)  || (n!="_id" && l==undefined)){
          delete value[id]
          elim= true
        }
      }



      if(!elim){

        if(val instanceof Array){
          for(var o of val){
            if(typeof o=="object")
              Query.projection(o, projection, mode, n)
          }
        }
        else if(typeof val=="object"){
          Query.projection(val, projection, mode, n)
        }

      }



    }

  }

  static compare(a, b){


    // Comparar el valor a con el b
    // pero teniendo en cuenta ciertos operadores
    val= true
    var o, p

    if(a instanceof Array && b){
      // Crear una copia sin $exists
      p={}
      for(var id in b){
        p[id]= b[id]
      }
      delete p.$exists
      b=p
    }


    if(a instanceof Array && b instanceof Array){
      if(b.length!=a.length)
        return false


      for(var i=0;i<a.length;i++){
        if(!this.compare(a[i], b[i]))
          return false
      }
      return true

    }

    if(a instanceof Array){
      for(var c of a){
      if(this.compare(c, b))
          return true
      }
      return false
    }




    if(b instanceof RegExp){
      return b.test(a?a.toString():null)
    }
    if(typeof b== "object"){

      for(var id in b){

        if(id=="$lt")
          val= a<b[id]

        else if(id=="$lte")
          val= a<=b[id]

        else if(id=="$gt")
          val= a>b[id]

        else if(id=="$gte")
          val= a>=b[id]

        else if(id=="$in")
          val= Query.compare(b[id], a)

        else if(id=="$nin")
          val= !Query.compare(b[id], a)

        else if(id=="$ne")
          val= a!=b[id]

        else if(id=="$exists")
          val= (a!==null && a!==undefined)==b[id]

        else if(id.startsWith("$"))
          throw new core.System.Exception("No se pudo reconocer el operador especial " + id)

        else{
          o= b[id]
          val= Query.compare(a?a[id]: null, o)
        }

        if(!val)
          break
      }
    }
    else{
      if(typeof a=="string" && typeof b=="string")
        val= a.toUpperCase()==b.toUpperCase()
      else
        val=  a==b
    }

    return val
  }
  /*
  static compareArrayToArray(a,b){
    var ok=0
    for(var c of a){
      for(var d of b){
        if(Query.compare(d,c)){
          ok++
          break
        }
      }
    }
    return ok==a.length
  }


  static compareArray(a,val){
    if(val &&val.$size){
      return a.length==val.$size
    }
    for(var c of a){
      if(Query.compare(c,val))
        return true
    }
    return false
  }
  */
  analize(conditions,index=0){

    var values=this.values, code=[], parts, str, y
    code.push("a"+index+": function(self){ ")
    if(this.$update||this.$remove)
      code.push("self=self.data;")
    for(var id in conditions){
      //vw.info(id,conditions[id])
      val= conditions[id]
      if(val){
        values.push(val)
        if(id=="$or"){
          str="v="
          y=0
          for(var query of val){
            index++
            this.analize(query, (index+y+1))

            str+= (y>0?"||": "") + "q.a"+(index+y+1)+"(self)"
            y++
          }
          code.push(str)
          code.push("if(!v) return false")

        }

        else if(id=="$and"){
          str="v="
          y=0
          for(var query of val){
            //index++
            this.analize(query, index+y+1)

            str+= (y>0?"&&": "") + "q.a"+(index+y+1)+"(self)"
            y++
          }
          code.push(str)
          code.push("if(!v) return false")

        }
        else if(id=="$not"){
          this.analize(val, index+1)
          code.push("v= !q.a"+(index+1)+"(self)")
          code.push("if(!v) return false")
        }

        else{

          /*
          if(val.$and || val.$or || val.$not || val.$where){
            this.analize(val, index+1)
            code.push("v= !a"+(index+1)+"()")
          }*/

          parts= id.split(".")

          code.push("a= self")
          if(parts.length==1){
            for(var i=0;i<parts.length;i++){
              code.push("a= a?a[" + JSON.stringify(parts[i]) + "]:null")
            }
            code.push("c=values["+(values.length-1)+"]")
            code.push("v= Query.compare(a,c)")
          }
          else{

            // En caso de que sea más de una parte puede que se necesite comparar por array
            code.push("c=values["+(values.length-1)+"]")
            values.push(this.analizeParts(parts))
            code.push("v= Query.gcompare(a, values["+values.length-1+"], c)")

          }
          code.push("if(!v) return false")

        }


      }



    }
    code.push("return true")
    code.push("}")
    this.funcs.push(code)


  }

  analizeSort(condition){
    var code= [], parts, val
    code.push("s0: function(y,z){")
    //vw.info("SORTING ------")
    for(var id in condition){
      parts=id.split(".")
      val= condition[id]

      //code.push("a= y")
      for(var i=0;i<parts.length;i++){
        if(i==0)
          code.push("a= y?y."+parts[i] + ":null")
        else
          code.push("a= a?a."+parts[i] + ":null")
      }

      for(var i=0;i<parts.length;i++){
        if(i==0)
          code.push("b= z?z."+parts[i] + ":null")
        else
          code.push("b= b?b."+parts[i] + ":null")
      }

      code.push("if(a>b) return " + (val>0 ?  1 : -1) )
      code.push("if(a<b) return " + (val>0 ? -1 :  1) )

    }
    code.push("return 0")
    code.push("}")
    this.funcs.push(code)
  }


  find(condition, projection){

    if(condition && Object.keys(condition).length>0){
      this.analize(condition)
    }

    if(projection && Object.keys(projection).length>0){
      this.analizeProjection(projection)
    }

  }

  update(condition, update, options){


    this.$update= update
    this.$updateOptions= options

    if(condition && Object.keys(condition).length>0){
      this.analize(condition)
    }

  }


  remove(condition, options){


    this.$remove= true
    this.$updateOptions= options

    if(condition && Object.keys(condition).length>0){
      this.analize(condition)
    }

  }

  analizeProjection(projection){
    var val,mode, idmode
    for(var id in projection){
      val= projection[id]
      if(val!=0&&val!=1)
        throw new core.System.Exception("La proyección no es válida")

      if(mode!=undefined&& mode!=val)
        throw new core.System.Exception("La proyección no es válida. No se puede usar modo eliminar y modo agregar de manera simultánea")

      if(id!="_id")
        mode= val|0
      else
        idmode= val|0
    }

    if(mode==undefined)
      mode=idmode

    this.$projection=  {
      mode,
      projection
    }

  }

  sort(condition){
    //vw.info("SORTING-...")
    if(Object.keys(condition).length>0){
      this.analizeSort(condition)
      this.$sorting= true
    }
  }



  /* async */count(){
    return this.execute({count:true})
  }


  async execute({count}){


    var sto=this.table.$storage, update, remove
    var data=[], dataLength=0
    var oSkip= this.$skip|0
    var skip= this.$sorting?0:oSkip,l

    update= this.$update
    remove= this.$remove
    var analytics= !!update|| !!remove

    if(update){
      update= Query.processForUpdate(update)
      if(this.$updateOptions && !this.$updateOptions.multi)
        this.$limit= 1
    }

    if(this.funcs.length>0){
      this.generateCode()
    }

    await sto.readAndSplit({
      analytics,
      ondata:(rows)=>{

        if(this.q && this.q.a0)
          rows= rows.filter(this.q.a0)

        var maxLength= rows.length
        if(!this.$sorting && this.$limit){
          maxLength= Math.min(this.$limit -dataLength,maxLength)
        }

        if(count){
          dataLength+= maxLength
        }
        else{
          for(var i=skip;i<maxLength;i++){
            data.push(rows[i])
            dataLength++
          }
          if(skip>0)
            skip= Math.max(0, skip-maxLength)
        }

        if(this.$sorting){
          // Eliminar los innecesarios
          if(maxLength>0)
            data.sort(this.q.s0)

          if(this.$limit&& data.length>this.$limit){
            l=data.length-this.$limit
            for(var i=0;i<l;i++){
              data.pop()
            }
          }
        }



        if(!this.$sorting && data.length>this.$limit){
          return false
        }
      },
      groupcount: this.$sorting?50: (this.$limit || 30)
    })
    if(count)
      return dataLength

    if(oSkip>0 && this.$sorting){
      for(var i=0;i<Math.min(data.length, oSkip);i++){
        data.shift()
      }
    }


    if(this.$update){
      count= data.length
      if(data.length==0 && this.$updateOptions&& this.$updateOptions.upsert){
        data= Query.update({_id: Kawasa.Util.uniqueId()}, update)
        sto.insert(data)
        count++
      }
      else if(data.length>0){
        await this._update(sto, data, update)
      }
      return count
    }
    else if(this.$remove){

      count= data.length
      if(data.length>0){
        await this._remove(sto, data)
      }
      return count

    }
    else if(this.$projection){
      data= data.map(Query.createProjectionMap(this.$projection))
    }

    return this.$one?data[0]:data
  }


  _update(sto, data, update){
    var d
    vw.log(update)
    for(var i=0;i<data.length;i++){
      d= data[i]
      d.data= Query.update(d.data, update)
    }
    return sto.insert(data, {
      edit: true
    })
  }

  async _remove(sto, data){
    return sto.remove(data)
  }




  limit(limit){
    this.$limit= limit
    return this
  }




  generateCode(){
    var str= this.createCode()
    var q
    var values= this.values
    eval(str)
    this.q= q
  }


  createCode(){
    var str= "q={"
    var y=0
    for(var code of this.funcs){

      str+= code.join("\n")
      if(y!= (this.funcs.length-1))
        str+=","

      y++
    }
    str+="}"
    return str
  }


}
export default Query
