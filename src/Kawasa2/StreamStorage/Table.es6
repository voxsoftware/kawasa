
import Path from 'path'
var Fs= core.System.IO.Fs
var Kawasa= core.org.voxsoftware.Database.Kawasa
var space= new Buffer(50)
space.fill(0)

class Table{


  constructor(stream, stream2, table){
    this.$stream= stream
    this.$stream2= stream2
    this.$pending= []
    this.$table= table
    this.createDocument()
    this.readBuffer= new Buffer(9*1024)
  }


  queue(action){

    // Las acciones se hacen por medio de un sistema de colas
    // La razón es que al realziar  por ejemplo un insert
    // debe esperarse que se termine el insert para hacer otro ...
    action.task= new core.VW.Task()
    this.$pending.push(action)
    if(!this.$working)
      this.doQueue()
    return action.task
  }




  async doQueue(){

    this.$working= true
    var pending= this.$pending
    this.$pending= []
    var er
    try{
      for(var action of pending){
        try{
          action.task.result= await this.realize(action)
        }
        catch(e){
          action.task.exception= e
        }
        action.task.finish()
      }
    }
    catch(e){
      er= e
    }

    if(!er && this.$pending.length){
      this.doQueue()
      return
    }

    this.$working=false
    if(er){
      throw er
    }
  }


  waitQueue(){

    var tasks= this.$pending.map((x)=> x.task)
    return core.VW.Task.waitMany(tasks)

  }


  // Aunque no tenga el atributo async, es async debido a que devuelve promesas
  realize(action){
    if(action.type=="insert"){
      return this.insert(action.docs)
    }
    else if(action.type=="remove"){
      action.query.remove(action.args[0],action.args[1])
      return action.query.execute()
    }
    else if(action.type=="update"){
      action.query.update(action.args[0],action.args[1], action.args[2])
      return action.query.execute()
    }
    else if(action.type=="countall"){
      return this.count()
    }
  }


  createDocument(){
    this.$breader= new core.System.IO.BinaryReader(this.$stream)
    this.$bwriter= new core.System.IO.BinaryWriter(this.$stream)

    this.$breader2= new core.System.IO.BinaryReader(this.$stream2)
    this.$bwriter2= new core.System.IO.BinaryWriter(this.$stream2)

    var str
    // Verificar que esté lo necesario ...
    this.$stream.position=0
    vw.log(this.$stream.length)
    if(this.$stream.length==0){
      this.$bwriter.writeString("kaw#")
      //vw.log(this.$stream.length)
      this.$start= this.$stream.position
      this.$bwriter.writeUInt32(0)
      this.$bwriter.writeByte(10)
    }
    else{

      // Verificar que sea el archivo mencionado ...
      str= this.$breader.readString()
      if(str!="kaw#")
        throw new core.System.Exception("El archivo de tabla tiene un formato no válido")
      this.$start= this.$stream.position
    }

    this.$stream.position= this.$stream.length
  }




  async _añadirConteo(count){
    this.$stream.position= this.$start
    var c= (await this.$breader.readUInt32Async())+(count||1)
    this.$stream.position= this.$start
    await this.$bwriter.writeUInt32Async(c)
  }





  async findAll(){
    var data=[]
    await this.readAndSplit({
      ondata:(rows)=>{
        for(var i=0;i<rows.length;i++){
          data.push(rows[i])
        }
      },
      groupcount: 10000
    })
    return data
  }


  async remove(positions){
    var count=0
    for(var position of positions){
      // vw.warning("Removing ",position.idxstart)
      this.$stream.position= position.idxstart+1
      await this.$stream.writeByteAsync(199)
      count--
    }
    await this._añadirConteo(count)
  }

  readUIntSafe(tbuf){

    var num1, num2
    var res= tbuf[7]

    tbuf[7]= tbuf[6]
    tbuf[6]= tbuf[5]
    tbuf[5]= tbuf[4]
    tbuf[4]= tbuf[3]
    tbuf[3]= 0

    num1= tbuf.readUInt32LE(0)
    num2= tbuf.readUInt32LE(4)
    //vw.warning("NUM2: ", num2)

    tbuf[3]= tbuf[4]
    tbuf[4]= tbuf[5]
    tbuf[5]= tbuf[6]
    tbuf[6]= tbuf[7]
    tbuf[7]= res

    return (num1* 4294967295)+num2

  }


  async readAndSplit({ondata, groupcount,analytics,noData }){

    // Esta función  va leyendo la tabla y convirtiendo en objetos
    var search=-1, lead, endSearch,str,d, str2, pos1, pos2
    var buf=new Buffer(2), data=[], pos, bufr
    buf[0]=10
    buf[1]=200
    this.$stream.position=this.$start
    while(true){
      pos= this.$stream.position
      if(await this.readPortion()){
        search=-1
        if(lead)
          this.currentBuffer= Buffer.concat([lead, this.currentBuffer])

        //vw.warning("HERE")
        while((search=this.currentBuffer.indexOf(buf, search+1))>=0){

          endSearch=search+16//this.currentBuffer.indexOf(10, search+1)
          if(endSearch>0){

            // Obtener el documento:

            str= this.currentBuffer.slice(search+2, search+10)
            str2= this.currentBuffer.slice(search+9, search+17)

            pos1= this.readUIntSafe(str)
            pos2= this.readUIntSafe(str2)


            d=null
            if(!noData){
              if(!bufr || bufr.length<pos2-pos1){
                bufr= new Buffer(pos2-pos1)
                bufr.fill(0)
              }
              this.$stream2.position= pos1
              
              await this.$stream2.readAsync(bufr, 0, pos2-pos1)
              d=bufr.toString('utf8', 0, pos2-pos1)


              d= JSON.parse(d)
            }


            if(analytics){



              data.push({
                data: d,
                portionstart: pos,
                idxstart: pos+ search,
                idxend: endSearch+pos,
                start: pos1,
                end: pos2
              })
            }else{
              if(!noData)
                data.push(d)
            }
          }
          else{
            lead= new Buffer(this.currentBuffer.slice(search, this.currentBuffer.length))
          }
        }


        if(data.length>=groupcount){
          if(!ondata(data))
            break

          data=[]
        }

      }
      else{
        break
      }
    }

    if(data.length>0){
      ondata(data)
    }

  }


  async readPortion(){
    //var time= Date.now()

    this.readed= await this.$stream.readAsync(this.readBuffer, 0, this.readBuffer.length)
    this.currentBuffer= this.readBuffer.slice(0, this.readed)
    //vw.warning(Date.now()-time, "ms en leer")
    return this.readed>0
  }



  async insert(document /* or documents*/, {edit}){
    var documents, positions=[], er
    if(document instanceof Array){
      documents= document
    }
    else{
      documents=[document]
    }
    /*
    if(this.$table._db.dStream){
      this.$table._db.dStream.enableVirtualMode()
    }
    */


    var buffer,len, id,json, _id, slen, doc, after
    slen= this.$stream.length
    this.$stream.position=slen
    this.$stream2.position= this.$stream2.length

    try{
      after= 2
      for(var document of documents){
        if(edit){
          doc=document
          document= document.data
        }

        // INsertar el documento
        if(!document._id || !edit){
          id= Kawasa.Util.uniqueId()
          document._id= id
        }

        json= core.safeJSON.stringify(document)
        json+= "\n"
        // write document to file ...
        len= Buffer.byteLength(json)


        if(!buffer || buffer.length<len){
          buffer= new Buffer(len)
        }
        buffer.write(json)



        if(edit){
          //vw.info(doc.end, doc.start)
          if(len<=(doc.end-doc.start)){
            this.$stream2.position= doc.start
          }
          else{

            doc.start= this.$stream2.position= this.$stream2.length
          }
        }
        else{
          doc={
            start: this.$stream2.position
          }
        }
        await this.$bwriter2.writeBytesAsync(buffer, len)

        if(edit){
          this.$stream.position= doc.idxstart+2
          await this.$bwriter.writeUIntSafeAsync(doc.start,true)
          await this.$bwriter.writeUIntSafeAsync(doc.start+len,true)
        }
        else{
          //this.$stream.position= this.$stream.length
          vw.log("Inserting;",doc.start, len)
          await this.$bwriter.writeByteAsync(200)
          await this.$bwriter.writeUIntSafeAsync(doc.start,true)
          await this.$bwriter.writeUIntSafeAsync(doc.start+len,true)
          await this.$bwriter.writeByteAsync(10)

        }

      }
    }
    catch(e){
      // Rollback transaction ...
      if(this.$stream.setLengthAsync)
        await this.$stream.setLengthAsync(slen)
      else
        this.$stream.setLength(slen)
      er=e
    }

    if(er){
      throw er
    }


    if(!edit)
      await this._añadirConteo(documents.length)

  }


  async count(){
    this.$stream.position= this.$start
    return await this.$breader.readUInt32()
  }


}
export default Table
