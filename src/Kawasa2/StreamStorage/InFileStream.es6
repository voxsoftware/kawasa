var IO= core.System.IO
var debug= false

class InFileStream extends IO.Stream{

  constructor(dstream, fileinfo, datablock){
    this.$dstream= dstream
    this.$fileinfo= fileinfo
    this.$block= datablock
    this.$blockingMode=true
    this.$pos=0

  }

  get _stream(){
    if(this.$stream)
      return this.$stream
    else
      return this.$dstream.$stream
  }

  get _breader(){
    if(this.$breader)
      return this.$breader
    else
      return this.$dstream.$breader
  }

  get _bwriter(){
    if(this.$bwriter)
      return this.$bwriter
    else
      return this.$dstream.$bwriter
  }

  get length(){
    // Obtener la longitud
    this._stream.position= this.$fileinfo.final+this.$fileinfo.blockstart-14
    /*var buf=new Buffer(7)
    this._stream.read(buf,0,7)
    vw.log("Length buf:",buf)
    this._stream.position-=7
    */
    var c= this._breader.readUIntSafe(true)
    // vw.log(c)
    return c
  }

  getLengthAsync(){
    this._stream.position= this.$fileinfo.final+this.$fileinfo.blockstart-14
    return this._breader.readUIntSafeAsync(true)
  }


  get position(){
    return this.$pos
  }

  setLength(){
    throw new core.System.NotImplementedException()
  }

  setLengthAsync(num){
    this._stream.position= this.$fileinfo.final+this.$fileinfo.blockstart-14
    return this._bwriter.writeUIntSafeAsync(num,true)
  }

  set position(pos){
    if(pos>this.length|| pos<0){
      throw new core.System.Exception("La posición no es válida")
    }
    this.$pos= pos
  }


  async ubicate(len, ev={}, pos, create=false){

    if(pos==undefined){
    debug&&  vw.warning("Exscribiendo len:", len)
      block1= await this.ubicate(undefined, ev, this.$pos,create)
      block2= await this.ubicate(undefined, ev, this.$pos+len,create)

      return [block1, block2]
    }


    // Ubicar el block desde el que debe
    // empezar a leer
    var block= this.$block, cblock
    var index= 0,i
    while(block){
      block.realsize= block.size-8
      if(block.realsize +index >= pos){
        i= pos-index
        cblock= block
        break
      }
      index+= block.realsize
      if(!block.next && !ev.parsed){
          // Cargar todos los blocks en caso no se haya hecho ..
          await this.$dstream.getFileBlock({
            getFirst: true,
            fromBlock: block
          })
          ev.parsed= true
      }

      if(!block.next && create){
        block= await this.$dstream.createDataBlock(block)
        debug &&vw.warning("Escribiento otro bloque necesario para escribir")
      }
      else
        block= block.next
    }



    return {
      i: i,
      block: cblock
    }
  }


  ubicateSync(len, ev={}, pos, create=false){

    if(pos==undefined){
    debug&&  vw.warning("Exscribiendo len:", len)
      block1= this.ubicateSync(undefined, ev, this.$pos,create)
      block2= this.ubicateSync(undefined, ev, this.$pos+len,create)

      return [block1, block2]
    }


    // Ubicar el block desde el que debe
    // empezar a leer
    var block= this.$block, cblock
    var index= 0,i
    while(block){
      block.realsize= block.size-8
      if(block.realsize +index >= pos){
        i= pos-index
        cblock= block
        break
      }
      index+= block.realsize
      if(!block.next && !ev.parsed){
          // Cargar todos los blocks en caso no se haya hecho ..
          this.$dstream.getFileBlockSync({
            getFirst: true,
            fromBlock: block
          })
          ev.parsed= true
      }

      if(!block.next && create){
        block= this.$dstream.createDataBlockSync(block)
        debug &&vw.warning("Escribiento otro bloque necesario para escribir")
      }
      else
        block= block.next
    }



    return {
      i: i,
      block: cblock
    }
  }




  async readAsync(bytes, offset, count){

    if(this.$canread===false)
      throw new core.System.Exception("No tiene permisos de lectura sobre este flujo")

    var blocks= await this.ubicate(count)
    var readed=0, rd
    var block= blocks[0].block
    var initial= blocks[0].i
    while(block){

      this._stream.position= block.start+initial
      rd= await this._stream.readAsync(bytes, offset, Math.min(block.realsize-initial, count))
      initial= 0

      offset+=rd
      readed+=rd
      count-=rd
      this.$pos+=rd
      if(count<=0)
        break


      block= block.next

    }
    return readed
  }


  read(bytes, offset, count){

    if(this.$canread===false)
      throw new core.System.Exception("No tiene permisos de lectura sobre este flujo")

    var blocks= this.ubicateSync(count)
    var readed=0, rd
    var block= blocks[0].block
    var initial= blocks[0].i
    while(block){

      this._stream.position= block.start+initial
      rd= this._stream.read(bytes, offset, Math.min(block.realsize-initial, count))
      initial= 0

      offset+=rd
      readed+=rd
      count-=rd
      this.$pos+=rd
      if(count<=0)
        break


      block= block.next

    }
    return readed
  }


  async writeAsync(bytes, offset, count){

    if(this.$canwrite===false)
      throw new core.System.Exception("No tiene permisos de escritura sobre este flujo")

    var blocks= await this.ubicate(count,undefined,undefined,true)
    var readed=0, rd
    var block= blocks[0].block
    var initial= blocks[0].i
    while(block){

      this._stream.position= block.start+initial
      rd= await this._stream.writeAsync(bytes, offset, Math.min(block.realsize-initial, count))
      initial= 0

      offset+=rd
      readed+=rd
      count-=rd
      this.$pos+=rd
      if(count<=0)
        break


      block= block.next

    }

    var len= await this.getLengthAsync()
    this._stream.position= this.$fileinfo.final+this.$fileinfo.blockstart-14
    await  this._bwriter.writeUIntSafeAsync(len+readed,true)

    return readed
  }

  write(bytes, offset, count){

    if(this.$canwrite===false)
      throw new core.System.Exception("No tiene permisos de escritura sobre este flujo")

    var blocks= this.ubicateSync(count,undefined,undefined,true)
    var readed=0, rd
    var block= blocks[0].block
    var initial= blocks[0].i
    while(block){

      this._stream.position= block.start+initial
      rd= this._stream.write(bytes, offset, Math.min(block.realsize-initial, count))
      initial= 0

      offset+=rd
      readed+=rd
      count-=rd
      this.$pos+=rd
      if(count<=0)
        break


      block= block.next

    }

    var len= this.length
    this._stream.position= this.$fileinfo.final+this.$fileinfo.blockstart-14
    this._bwriter.writeUIntSafe(len+readed,true)

    return readed
  }


}
export default InFileStream
