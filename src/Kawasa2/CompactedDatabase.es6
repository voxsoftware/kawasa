var Kawasa= core.org.voxsoftware.Database.Kawasa
var Fs= core.System.IO.Fs
class CompactedDatabase extends Kawasa.Database{


  constructor(/* srting*/ file){

    // EL punto de partida de la base de datos es una carpeta efectivamente ...
    this.file= file
    this.dStream= new Kawasa.StreamStorage.DirectoryStream(file)

  }

  table(name){

    return this.getTable(name)

    // Crea una nueva tabla a partir de un nombre específico ...
    var table= new Kawasa.CompactedTable(name)
    table.db= this
    table.start()
    return table
  }

  async getTable(name){
    var table= new Kawasa.CompactedTable(name)
    table.db= this
    await table.start()
    return table
  }

}
export default CompactedDatabase
