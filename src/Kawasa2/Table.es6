
import Path from 'path'
var Fs= core.System.IO.Fs
var Kawasa= core.org.voxsoftware.Database.Kawasa


class Table{
  constructor(name){
    this.name= name
  }

  get db(){
    return this._db
  }


  set db(db){
    this._db= db
    this.fileData= Path.join(db.path, this.name + ".db")
    this.fileIndex= Path.join(db.path, this.name + ".idx")
    this.createDocument()
  }


  createDocument(){
    this.$stream2= new core.System.IO.FileStream(this.fileData,
      core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite)

    this.$stream= new core.System.IO.FileStream(this.fileIndex,
      core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite)

    this.$storage= new Kawasa.StreamStorage.Table(this.$stream,this.$stream2, this)
  }

  insert(document /* or documents*/){
    return this.$storage.queue({
      docs:document,
      type:"insert"
    })
  }

  find(args,projection){
    var q= new Kawasa.Query()
    q.table= this
    q.find(args,projection)
    return q
  }


  findOne(args){
    var q= new Kawasa.Query()
    q.table= this
    q.find(args)
    q.limit(1)
    q.$one= true
    return q
  }


  update(){
    var q= new Kawasa.Query()
    q.table= this

    return this.$storage.queue({
      query: q,
      args:arguments,
      type:"update"
    })


  }

  remove(){
    var q= new Kawasa.Query()
    q.table= this

    return this.$storage.queue({
      query: q,
      args:arguments,
      type:"remove"
    })


  }


  count(args){
    if(args && Object.keys(args)>0){
      return this.find(args).count()
    }

    return this.$storage.queue({

      type:"countall"
    })
  }

  waitQueue(){
    return this.$storage.waitQueue()
  }
}
export default Table
