
import Path from 'path'
var Fs= core.System.IO.Fs
var Kawasa= core.org.voxsoftware.Database.Kawasa


class CompactedTable extends Kawasa.Table{
  constructor(name){
    this.name= name
  }

  get db(){
    return this._db
  }


  set db(db){
    this._db= db
    this.fileData= this.name
    this.fileIndex= this.name+".idx"
    //this.createDocument()
  }


  async start(){
    this.$stream= await this._db.dStream.open(this.fileData,
      core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite)

    this.$storage= new Kawasa.StreamStorage.Table(this.$stream,this)
  }



}
export default CompactedTable
