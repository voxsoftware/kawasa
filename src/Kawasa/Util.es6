

class Util{

  static uniqueId(){
    var d= Date.now()
    var last= Util.last || d
    d= Util.last=  d>last?d: last+1
    return Util.prefix+ d.toString(36)
  }


}


if(!Util.prefix){
  Util.prefix= parseFloat((Math.random() * Date.now() ).toFixed(1)).toString(32)
  //Util.prefix= Util.prefix.replace(".","").substring(0,9)
  Util.prefix= process.pid+Util.prefix
  Util.prefix= Util.prefix.replace(".","").substring(0,14)
  if(Util.prefix.length<14){
    for(var i=Util.prefix.length;i<14;i++){
      Util.prefix+="w"
    }
  }

}

//Util.num=0
export default Util
