
var Stream = core.System.IO.Stream
var emptyBlock= new Buffer(6*1024)
emptyBlock.fill(0)
var blockSize= 6*1024
var Kawasa= core.org.voxsoftware.Database.Kawasa
var debug= false
class DirectoryStream {

  constructor(stream /* or file*/){

    if(!(stream instanceof Stream)){
      this.$file= stream
      stream= new core.System.IO.FileStream(stream, core.System.IO.FileMode.OpenOrCreate,
        core.System.IO.FileAccess.ReadWrite)
    }

    // DirectoryStream es un directorio completo dentro de un Stream
    // que puede ser por ejemplo un FileStream
    this.$readBuffer= new Buffer(blockSize)
    this.$stream= stream
    this.$breader= new core.System.IO.BinaryReader(stream)
    this.$bwriter= new core.System.IO.BinaryWriter(stream)
    this.start()

  }


  start(){
    var str
    if(this.$stream.length==0){
      // Iniciar el archivo ...
      this.$bwriter.writeString("#KawDs")
      //this.$bwriter.writeByte(10)
    }
    else{
      str= this.$breader.readString()
      if(str!="#KawDs")
        throw new core.System.Exception("El formato no es válido")
    //  this.$stream.position++
    }
    this.$start= this.$stream.position
  }


  async getFileBlock({getFirst, fromBlock, fromStart}){
    // Obtener el bloque actual en el que se está escribiendo los nombres de archivos ...


    var block, nextBlock, b, first,pos, rr
    first= fromBlock

    if(!first){
      first={
        size: blockSize,
        start: fromStart || this.$start
      }
    }
    block= first
    //vw.warning("Starting block:", first)



    while(true){
      //vw.log(this.$stream.position)
      rr= false
      pos= block.start + blockSize - 8
      if(this.virtualMode){
        if(this.$stream.length<=pos)
        // El modo virtual es solo para escritura así que
        // se toma como que no hay nextblock
          nextBlock= false
        else {
          rr= true
        }
      }
      if(rr){
        this.$stream.position= pos
        nextBlock= await this.$breader.readBooleanAsync()
      }

      debug&&vw.info("Hay otro bloque?", nextBlock)
      if(nextBlock){
        b= block
        block= {
          prev:block,
          size:blockSize,
          start: await this.$breader.readUIntSafeAsync(true)
        }
        b.next= block
      }
      else{
        break
      }
    }

    if(getFirst){
      block= first
    }
    /*
    if(block){
      await this.getCurrentPosition()
    }*/
    return block
  }



  getFileBlockSync({getFirst, fromBlock, fromStart}){
    // Obtener el bloque actual en el que se está escribiendo los nombres de archivos ...


    var block, nextBlock, b, first, rr, pos
    first= fromBlock

    if(!first){
      first={
        size: blockSize,
        start: fromStart || this.$start
      }
    }
    block= first
    //vw.warning("Starting block:", first)



    while(true){
      //vw.log(this.$stream.position)
      rr= false
      pos= block.start + blockSize - 8
      if(this.virtualMode){
        if(this.$stream.length<=pos)
        // El modo virtual es solo para escritura así que
        // se toma como que no hay nextblock
          nextBlock= false
        else {
          rr= true
        }
      }
      if(rr){
        this.$stream.position= pos
        nextBlock= this.$breader.readBoolean()
      }
      debug&&vw.info("Hay otro bloque?", nextBlock)
      if(nextBlock){
        b= block
        block= {
          prev:block,
          size:blockSize,
          start: this.$breader.readUIntSafe(true)
        }
        b.next= block
      }
      else{
        break
      }
    }

    if(getFirst){
      block= first
    }
    /*
    if(block){
      await this.getCurrentPosition()
    }*/
    return block
  }



  async getCurrentPosition(block){
    this.$stream.position= block.start
    var i=-1, y=1
    await this.$stream.read(this.$readBuffer, 0, block.size-8)
    while((i=this.$readBuffer.indexOf(10,i+1))>=0){
      y=i+1
    }
    block.current=y
  }



  async readFiles({onfind,onlast}){
    //var len= Buffer.bufferLength(file)
    var flen= this.$stream.length, find
    if(flen<blockSize){
      //emptyBlock[0]=10
      this.$stream.position= this.$start
      //await this.$stream.write(emptyBlock,0,emptyBlock.length)
      await this.createFileBlock()
    }
    var buf= new Buffer(2),i,y,file2,tbuf
    buf[0]=10
    buf[1]=200
    var num1,num2

    // Obtener el primer bloque ...
    var block= this.$firstFileBlock
    if(!block){
      block= this.$firstFileBlock= await this.getFileBlock({
       getFirst: true
      })

    }
    else{
      await this.getFileBlock({
       fromBlock: block
      })
    }


    var cblock
    while(block){
      //vw.warning("LOOPantes")
      //vw.warning("LOOP",block.start)
      this.$stream.position= block.start
      cblock= block
      readed=  await this.$stream.read(this.$readBuffer, 0, blockSize)
      if(!readed)
        break


      i=-1
      var size1, size2
      while((i= this.$readBuffer.indexOf(buf,i+1))>=0){

        // Encontrar el siguiente bloque ...
        y= this.$readBuffer.indexOf(10, i+2)
        if(y>=0){

          file2= this.$readBuffer.toString( 'utf8', i+2, y-14)
          // Leer donde empieza el primer bloque ...
          if(!tbuf)
            tbuf= new Buffer(8)

            this.$readBuffer.copy(tbuf, 0, y-7, y-4)
            this.$readBuffer.copy(tbuf, 4, y-4, y)
            tbuf[3]=0


            num1= tbuf.readUInt32LE(0)
            num2= tbuf.readUInt32LE(4)
            size1= (num1* 4294967295)+num2

            this.$readBuffer.copy(tbuf, 0, y-14, y-11)
            this.$readBuffer.copy(tbuf, 4, y-11, y-7)
            tbuf[3]=0



            num1= tbuf.readUInt32LE(0)
            num2= tbuf.readUInt32LE(4)
            size2= (num1* 4294967295)+num2



          find={
            blockstart: block.start,
            initial: i,
            final: y,
            file: file2,
            size:size2,
            dataBlock: size1
          }


          if(onfind){
            if(onfind(find)===false)
              return false
          }
        }

      }
      block= block.next
    }


    if(onlast && cblock){
      onlast(cblock)
    }

  }


  async readAllFiles(){
    var data= []
    await this.readFiles({
      onfind: (find)=>{

        data.push({
          file:find.file,
          size:find.size
        })
      }
    })
    return data
  }




  async open(file, mode, access){
    var b, block, str
    var create= true

    if(mode===undefined)
      mode= core.System.IO.FileMode.Open

    if((mode&core.System.IO.FileMode.Create)!=core.System.IO.FileMode.Create){
      if((mode&core.System.IO.FileMode.OpenOrCreate)!=core.System.IO.FileMode.OpenOrCreate)
        create= false
    }


    // Buscar el archivo en el stream
    await this.readFiles({
      onfind: (find)=>{

        if(find.file==file){
          b= find
          return false
        }

      },
      onlast: (l)=>{
        block= l
      }
    })

    var datablock

    if(!b){

      // Crear el archivo
      if(!create){
        throw new core.System.IO.FileNotFoundException(file)
      }
      else{

        await this.getCurrentPosition(block)
        str= new Buffer(file)
        //vw.warning("Current size and position: ", block.size, block.current)
        if((1+str.length + 1 + 8) > (block.size - block.current)){
          // Crear un nuevo bloque ...
          block= await this.createFileBlock(block)
          block.current= 1
        }
        //fileblockIndex= block.current

        // Escribir el nuevo bloque de datos ...
        datablock= await this.createDataBlock()

        this.$stream.position= block.current+block.start

        b= {
          initial: block.current,
          file: file,
          blockstart: block.start,
          dataBlock: datablock.start
        }
        this.$bwriter.writeByte(200)
        await this.$stream.writeAsync(str,0,str.length)
        await this.$bwriter.writeUIntSafeAsync(0,true)
        await this.$bwriter.writeUIntSafeAsync(datablock.start,true)
        this.$bwriter.writeByte(10)
        b.final= this.$stream.position-1-block.start
        //vw.info("FINALB:",b.final)

      }

    }
    else{


      datablock= await this.getFileBlock({
        getFirst:true,
        fromStart: b.dataBlock
      })

    }

    var f=  new Kawasa.StreamStorage.InFileStream(this, b, datablock)
    if((mode&core.System.IO.FileMode.Truncate)==core.System.IO.FileMode.Truncate)
      await f.setLengthAsync(0)


    if(access===undefined)
      access= core.System.IO.FileAccess.Read

    if((access&core.System.IO.FileAccess.Read)!=core.System.IO.FileAccess.Read)
  //if(!(access & (core.System.IO.FileAccess.Read)))
      f.$canread= false

    if((access&core.System.IO.FileAccess.Write)!=core.System.IO.FileAccess.Write){
      //vw.log(access)
      f.$canwrite=false
      //process.exit(0)
    }
      //f.$canwrite= false

    return f


  }


  async createFileBlock(prev){
    //vw.info("Creating block: ")
    var block= await this.createDataBlock(prev)
    this.$stream.position= block.start
    this.$bwriter.writeByte(10)
    block.current= 1
    return block
  }



  async createDataBlock(prev){



    var len= this.$stream.position= this.virtualMode?this.$length: this.$stream.length

    if(!this.virtualMode){
      emptyBlock[0]=0
      await this.$stream.writeAsync(emptyBlock, 0, emptyBlock.length)
    }

    else{
      this.$length+= emptyBlock.length
    }




    var block={
      size: blockSize,
      start: len
    }

    if(prev){

      if(this.virtualMode){
        this.$virtual.push({
          prev:  prev,
          block: block
        })
      }
      else{
        await this.writeStart(prev,block)
      }

      prev.next= block
      block.prev= prev
    }
    return block

  }


  createDataBlockSync(prev){



    var len= this.$stream.position= this.virtualMode?this.$length: this.$stream.length

    if(!this.virtualMode){
      emptyBlock[0]=0
      this.$stream.write(emptyBlock, 0, emptyBlock.length)
    }

    else{
      this.$length+= emptyBlock.length
    }




    var block={
      size: blockSize,
      start: len
    }

    if(prev){

      if(this.virtualMode){
        this.$virtual.push({
          prev:  prev,
          block: block
        })
      }
      else{
        this.writeStart(prev,block)
      }

      prev.next= block
      block.prev= prev
    }
    return block

  }

  async writeStart(prev, block){
    this.$stream.position= prev.start+prev.size-8
    this.$bwriter.writeByte(1)
    debug&&vw.warning("Writing uintsafe", block.start)
    await this.$bwriter.writeUIntSafeAsync(block.start,true)
  }

  writeStartSync(prev, block){
    this.$stream.position= prev.start+prev.size-8
    this.$bwriter.writeByte(1)
    debug&&vw.warning("Writing uintsafe", block.start)
    this.$bwriter.writeUIntSafe(block.start,true)
  }

  enableVirtualMode(){
    this.$length= this.$stream.length
    this.virtualMode= true
    this.$virtual=[]
  }

  async disableVirtualMode(){
    var pos= this.$stream.position
    this.$stream.position= this.$stream.length
    if(this.$length!==undefined){


      var dif= this.$length-this.$stream.length, wr
      //vw.warning("dif",dif)
      /*vw.log("Diferencia: ",dif)
      emptyBlock[0]=0
      while(dif>0){
        wr=await this.$stream.writeAsync(emptyBlock, 0, Math.min(emptyBlock.length, dif) )
        dif-= wr
      }*/
      if(dif>0){
        this.$stream.setLength(this.$length)
      }

      // Escribir los virtual ...
      for(var v of this.$virtual){
        await this.writeStart(v.prev, v.block)
      }
      this.$virtual=undefined
      this.$stream.position=pos

    }
    this.$length=undefined
    this.virtualMode= false
  }

  disableVirtualModeSync(){
    var pos= this.$stream.position
    this.$stream.position= this.$stream.length
    if(this.$length!==undefined){

      var dif= this.$length-this.length, wr
      if(dif>0){
        this.$stream.setLength(this.$length)
      }


      // Escribir los virtual ...
      for(var v of this.$virtual){
        this.writeStartSync(v.prev, v.block)
      }
      this.$virtual=undefined
      this.$stream.position=pos

    }
    this.$length=undefined
    this.virtualMode= false
  }



}
export default DirectoryStream
