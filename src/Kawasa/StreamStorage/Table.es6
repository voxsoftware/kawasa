
import Path from 'path'
var Fs= core.System.IO.Fs
var Kawasa= core.org.voxsoftware.Database.Kawasa
var space= new Buffer(50)
space.fill(0)

class Table{


  constructor(stream, table){
    this.$stream= stream
    this.$pending= []
    this.$table= table
    this.createDocument()
    this.readBuffer= new Buffer(9*1024)
  }


  queue(action){

    // Las acciones se hacen por medio de un sistema de colas
    // La razón es que al realziar  por ejemplo un insert
    // debe esperarse que se termine el insert para hacer otro ...
    action.task= new core.VW.Task()
    this.$pending.push(action)
    if(!this.$working)
      this.doQueue()
    return action.task
  }




  async doQueue(){

    this.$working= true
    var pending= this.$pending
    this.$pending= []
    var er
    try{
      for(var action of pending){
        try{
          action.task.result= await this.realize(action)
        }
        catch(e){
          action.task.exception= e
        }
        action.task.finish()
      }
    }
    catch(e){
      er= e
    }

    if(!er && this.$pending.length){
      this.doQueue()
      return
    }

    this.$working=false
    if(er){
      throw er
    }
  }


  waitQueue(){

    var tasks= this.$pending.map((x)=> x.task)
    return core.VW.Task.waitMany(tasks)

  }


  // Aunque no tenga el atributo async, es async debido a que devuelve promesas
  realize(action){
    if(action.type=="insert"){
      return this.insert(action.docs)
    }
    else if(action.type=="remove"){
      action.query.remove(action.args[0],action.args[1])
      return action.query.execute()
    }
    else if(action.type=="update"){
      action.query.update(action.args[0],action.args[1], action.args[2])
      return action.query.execute()
    }
    else if(action.type=="countall"){
      return this.count()
    }
  }


  createDocument(){
    this.$breader= new core.System.IO.BinaryReader(this.$stream)
    this.$bwriter= new core.System.IO.BinaryWriter(this.$stream)

    var str
    // Verificar que esté lo necesario ...
    this.$stream.position=0
    vw.log(this.$stream.length)
    if(this.$stream.length==0){
      this.$bwriter.writeString("kaw#")
      //vw.log(this.$stream.length)
      this.$start= this.$stream.position
      this.$bwriter.writeUInt32(0)
      this.$bwriter.writeByte(10)
    }
    else{

      // Verificar que sea el archivo mencionado ...
      str= this.$breader.readString()
      if(str!="kaw#")
        throw new core.System.Exception("El archivo de tabla tiene un formato no válido")
      this.$start= this.$stream.position
    }

    this.$stream.position= this.$stream.length
  }




  async _añadirConteo(count){
    this.$stream.position= this.$start
    var c= (await this.$breader.readUInt32Async())+(count||1)
    this.$stream.position= this.$start
    await this.$bwriter.writeUInt32Async(c)
  }


  /*
  async readId(uid){
    var buf= new Buffer(uid.length+2), lead
    buf[0]=10
    buf[1]=200
    buf.write(uid, 2)
    var search, endSearch, str
    this.$stream.position= this.$start+4
    while(true){
      if(await this.readPortion()){
        if(lead)
          this.currentBuffer= Buffer.concat([lead, this.currentBuffer])

        // Buscar el uid ...
        search=this.currentBuffer.indexOf(buf)
        if(search>=0){

          endSearch= this.currentBuffer.indexOf(10, search+buf.length)
          if(endSearch>0){

            // Hacer un slice y leer ...
            str= this.currentBuffer.slice(search+buf.length, endSearch).toString()
            return JSON.parse(str)
          }
          else{
            lead= new Buffer(this.currentBuffer.slice(search, this.currentBuffer.length))
          }
        }
        else{
          // Buscar el último \n
          search=-1
          endSearch=0
          while((search=this.currentBuffer.indexOf(10, search+1))>=0){
            endSearch= search
          }
          lead= new Buffer(this.currentBuffer.slice(endSearch))
        }
      }
      else{
        break
      }
    }

    return null
  }*/



  async findAll(){
    var data=[]
    await this.readAndSplit({
      ondata:(rows)=>{
        for(var i=0;i<rows.length;i++){
          data.push(rows[i])
        }
      },
      groupcount: 10000
    })
    return data
  }


  async remove(positions){
    var count=0
    for(var position of positions){
      vw.warning("Removing ",position.start)
      this.$stream.position= position.start+1
      await this.$stream.writeByteAsync(199)
      count--
    }
    await this._añadirConteo(count)
  }


  async readAndSplit({ondata, groupcount,analytics,noData }){

    // Esta función  va leyendo la tabla y convirtiendo en objetos
    var search=-1, lead, endSearch,str,d
    var buf=new Buffer(2), data=[], pos
    buf[0]=10
    buf[1]=200
    this.$stream.position=this.$start
    while(true){
      pos= this.$stream.position
      if(await this.readPortion()){
        search=-1
        if(lead)
          this.currentBuffer= Buffer.concat([lead, this.currentBuffer])

        //vw.warning("HERE")
        while((search=this.currentBuffer.indexOf(buf, search+1))>=0){

          endSearch=this.currentBuffer.indexOf(10, search+1)
          if(endSearch>0){
            // Obtener el documento:
            str= this.currentBuffer.slice(search+2, endSearch).toString()
            if(analytics){

              d=noData?null:JSON.parse(str)

              data.push({
                data: d,
                portionstart: pos,
                start: pos+ search,
                end: endSearch+pos
              })
            }else{
              if(!noData)
                data.push(JSON.parse(str))
            }
          }
          else{
            lead= new Buffer(this.currentBuffer.slice(search, this.currentBuffer.length))
          }
        }


        if(data.length>=groupcount){
          if(!ondata(data))
            break

          data=[]
        }

      }
      else{
        break
      }
    }

    if(data.length>0){
      ondata(data)
    }

  }


  async readPortion(){
    //var time= Date.now()

    this.readed= await this.$stream.readAsync(this.readBuffer, 0, this.readBuffer.length)
    this.currentBuffer= this.readBuffer.slice(0, this.readed)
    //vw.warning(Date.now()-time, "ms en leer")
    return this.readed>0
  }



  async insert(document /* or documents*/, {edit}){
    var documents, positions=[], er
    if(document instanceof Array){
      documents= document
    }
    else{
      documents=[document]
    }
    /*
    if(this.$table._db.dStream){
      this.$table._db.dStream.enableVirtualMode()
    }
    */


    var buffer,len, id,json, _id, slen, doc, after
    slen= this.$stream.length
    this.$stream.position=slen
    //_id= new Buffer(23)

    try{
      after= 2
      for(var document of documents){
        if(edit){
          doc=document
          document= document.data
        }

        // INsertar el documento
        if(!document._id || !edit){
          id= Kawasa.Util.uniqueId()
          document._id= id
        }

        json= core.safeJSON.stringify(document)
        // write document to file ...
        len= Buffer.byteLength(json)

        if(edit && len+2 < (doc.end-doc.start)){
          after= 4
        }

        if(!buffer || buffer.length<(len+after)){
          buffer= new Buffer(len+after)
        }
        buffer.write(json,1,len+1)
        buffer[len+1]=10
        if(after>2){
          buffer[len+2]= 199
          buffer[len+3]= 10
        }
        buffer[0]=200


        if(edit){
          vw.info(doc.end, doc.start)
          if(len+2<=(doc.end-doc.start)){
            this.$stream.position= doc.start+1
          }
          else{
            //this.$stream.writeByte(199)
            positions.push(doc.start+1)
            this.$stream.position= this.$stream.length
          }
        }
        await this.$bwriter.writeBytesAsync(buffer, len+after)

      }
    }
    catch(e){
      // Rollback transaction ...
      if(this.$stream.setLengthAsync)
        await this.$stream.setLengthAsync(slen)
      else
        this.$stream.setLength(slen)
      er=e
    }

    if(er){
      throw er
    }


    len= this.$stream.length
    for(var position of positions){
      this.$stream.position= position
      this.$stream.writeByte(199)
      if(position+1>=len){
        this.$stream.writeByte(10)
      }

    }

    if(!edit)
      await this._añadirConteo(documents.length)

  }




  async count(){
    this.$stream.position= this.$start
    return await this.$breader.readUInt32()
  }




}
export default Table
