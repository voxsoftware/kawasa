var Kawasa= core.org.voxsoftware.Database.Kawasa
var Fs= core.System.IO.Fs
class Database{


  constructor(/* srting*/ folder){

    // EL punto de partida de la base de datos es una carpeta efectivamente ...
    this.path= folder
    if(!Fs.sync.exists(folder))
      Fs.sync.mkdir(folder)

  }

  table(name){

    // Crea una nueva tabla a partir de un nombre específico ...
    var table= new Kawasa.Table(name)
    table.db= this
    return table
  }

}
export default Database
