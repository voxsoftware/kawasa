var Kawasa= core.org.voxsoftware.Database.Kawasa

class Sesion{

  constructor(options){
    this.options=options
  }

  async handle(args){

    var cookie= args.request.cookie, c, time
    if(!cookie)
      cookie= new core.VW.Http.ServerCookie()
    var id=cookie.get("kawasa_id")
    if(id){
      c= await this.options.collection.findOne({
        _id: id
      }).execute()

      if(c && c.maxAge!==undefined && c.maxAge>0){
        // Verificar que no se haya vencido ...
        time= parseInt(Date.now()-c.accesed)
        if(time>c.maxAge){
            id=undefined
        }
      }
    }
    if(!c)
      id=undefined

    if(!id){
      c= {
        data: {},
        updated: Date.now(),
        created: Date.now(),
        accesed: Date.now(),
        maxAge: this.options.maxAge || 0
      }

      await this.options.collection.insert(c)
      cookie.set("kawasa_id", c._id)
    }
    else{
      // Actualizar accesed time ...
      await this.options.collection.update({
        _id: id
      },{
        $set:{
          accesed: Date.now()
        }
      })
    }

    if(!id)
      args.response.setHeader( "set-cookie", cookie.toStr("kawasa_id"))

    c.data=c.data||{}
    args.request.session= c.data
    args.request.cookie= cookie
    if(args.continue)
      return await args.continue()
  }


  async save(args){
    var cookie= args.request.cookie
    if(!cookie)
      return
    var id= cookie.get("kawasa_id")
    await this.options.collection.update({
      _id: id
    },{
      $set:{
        accesed: Date.now(),
        data: args.request.session,
        updated: Date.now()
      }
    })

    if(args.continue)
      return await args.continue()
  }

  async destroy(args){

    var cookie= args.request.cookie
    var id= cookie.get("kawasa_id")
    if(!id)
      return

    await this.options.collection.remove({
      _id: id
    })

    if(args.continue)
      return await args.continue()

  }

}


export default Sesion
