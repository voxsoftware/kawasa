var $mod$3 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var Kawasa = core.org.voxsoftware.Database.Kawasa;
{
    var Table = function Table() {
        Table.$constructor ? Table.$constructor.apply(this, arguments) : Table.$superClass && Table.$superClass.apply(this, arguments);
    };
    Table.$constructor = function (name) {
        this.name = name;
    };
    Table.prototype.__defineGetter__('db', function () {
        return this._db;
    });
    Table.prototype.__defineSetter__('db', function (db) {
        this._db = db;
        this.fileData = $mod$3.default.join(db.path, this.name + '.db');
        this.fileIndex = $mod$3.default.join(db.path, this.name + '.idx');
        this.createDocument();
    });
    Table.prototype.createDocument = function () {
        this.$stream = new core.System.IO.FileStream(this.fileData, core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite);
        this.$storage = new Kawasa.StreamStorage.Table(this.$stream, this);
    };
    Table.prototype.insert = function (document) {
        return this.$storage.queue({
            docs: document,
            type: 'insert'
        });
    };
    Table.prototype.find = function (args, projection) {
        var q = new Kawasa.Query();
        q.table = this;
        q.find(args, projection);
        return q;
    };
    Table.prototype.findOne = function (args) {
        var q = new Kawasa.Query();
        q.table = this;
        q.find(args);
        q.limit(1);
        q.$one = true;
        return q;
    };
    Table.prototype.update = function () {
        var q = new Kawasa.Query();
        q.table = this;
        return this.$storage.queue({
            query: q,
            args: arguments,
            type: 'update'
        });
    };
    Table.prototype.remove = function () {
        var q = new Kawasa.Query();
        q.table = this;
        return this.$storage.queue({
            query: q,
            args: arguments,
            type: 'remove'
        });
    };
    Table.prototype.count = function (args) {
        if (args && Object.keys(args) > 0) {
            return this.find(args).count();
        }
        return this.$storage.queue({ type: 'countall' });
    };
    Table.prototype.waitQueue = function () {
        return this.$storage.waitQueue();
    };
}
exports.default = Table;