var Kawasa = core.org.voxsoftware.Database.Kawasa;
var $mod$1 = core.VW.Ecma2015.Utils.module(require('path'));
{
    var Query = function Query() {
        Query.$constructor ? Query.$constructor.apply(this, arguments) : Query.$superClass && Query.$superClass.apply(this, arguments);
    };
    Query.$constructor = function () {
        this.clear();
    };
    Query.prototype.clear = function () {
        this.funcs = [];
        this.values = [];
        this.q = null;
    };
    Query.analizeParts = function (parts) {
        var p = [];
        for (var i = 0; i < parts.length; i++) {
            p.push({
                text: parts[i],
                number: /^\d+$/.test(parts[i])
            });
        }
        return p;
    };
    Query.prototype.analizeParts = function (a) {
        return Query.analizeParts(a);
    };
    Query.gcompare = function (a, parts, b, indexStart) {
        if (indexStart === undefined) {
            indexStart = 0;
        }
        var part;
        for (var i = indexStart; i < parts.length; i++) {
            part = parts[i];
            a = a[part.text];
            if (a && i < parts.length - 1 && !parts[i + 1].number & a instanceof Array) {
                {
                    var c;
                    var $_iterator2 = regeneratorRuntime.values(a), $_normal2 = false, $_err2;
                    try {
                        while (true) {
                            var $_step2 = $_iterator2.next();
                            if ($_step2.done) {
                                $_normal2 = true;
                                break;
                            }
                            {
                                c = $_step2.value;
                                if (Query.gcompare(c, parts, b, i + 1))
                                    return true;
                            }
                        }
                    } catch (e) {
                        $_normal2 = false;
                        $_err2 = e;
                    }
                    try {
                        if (!$_normal2 && $_iterator2['return']) {
                            $_iterator2['return']();
                        }
                    } catch (e) {
                    }
                    if ($_err2) {
                        throw $_err2;
                    }
                }
                return false;
            }
            if (!a)
                break;
        }
        return Query.compare(a, b);
    };
    Query.ucompare = function (mode, a, parts, b, indexStart) {
        if (indexStart === undefined) {
            indexStart = 0;
        }
        var part, i, slice;
        vw.log(parts);
        for (var i = indexStart; i < parts.length - 1; i++) {
            part = parts[i];
            a = a[part.text] || {};
            if (a && i < parts.length - 1 && !parts[i + 1].number & a instanceof Array) {
                {
                    var c;
                    var $_iterator3 = regeneratorRuntime.values(a), $_normal3 = false, $_err3;
                    try {
                        while (true) {
                            var $_step3 = $_iterator3.next();
                            if ($_step3.done) {
                                $_normal3 = true;
                                break;
                            }
                            {
                                c = $_step3.value;
                                Query.ucompare(mode, c, parts, b, i + 1);
                            }
                        }
                    } catch (e) {
                        $_normal3 = false;
                        $_err3 = e;
                    }
                    try {
                        if (!$_normal3 && $_iterator3['return']) {
                            $_iterator3['return']();
                        }
                    } catch (e) {
                    }
                    if ($_err3) {
                        throw $_err3;
                    }
                }
                return;
            }
            if (!a)
                break;
        }
        if (mode == '$set')
            a[parts[parts.length - 1].text] = b;
        else if (mode == '$unset')
            delete a[parts[parts.length - 1].text];
        else if (mode == '$inc')
            a[parts[parts.length - 1].text] = a[parts[parts.length - 1].text] + b;
        else if (mode == '$max')
            a[parts[parts.length - 1].text] = Math.max(a[parts[parts.length - 1].text], b);
        else if (mode == '$min')
            a[parts[parts.length - 1].text] = Math.min(a[parts[parts.length - 1].text], b);
        else if (mode == '$push') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array) {
                if (typeof b == 'object' && b.$slice) {
                    slice = b.$slice;
                    if (!b.$each) {
                        throw new core.System.Exception('Debe especificar la propiedad each junto a slice');
                    }
                    b.$each = b.$each.slice(0, Math.min(0, slice - a.length));
                }
                if (typeof b == 'object' && b.$each) {
                    for (var z = 0; z < b.$each.lenght; z++) {
                        a.push(b.$each[z]);
                    }
                } else {
                    a.push(b);
                }
                if (a.length > slice) {
                    a = a.slice(0, slice);
                }
            }
        } else if (mode == '$addToSet') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array && a.indexOf(b) < 0)
                a.push(b);
        } else if (mode == '$pop') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array) {
                if (b > 0)
                    a.pop();
                else if (b < 0)
                    a.shift();
            }
        } else if (mode == '$pull') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array) {
                if (typeof b == 'object' && b.$in) {
                    for (var z = 0; z < b.$in.lenght; z++) {
                        while (i = a.indexOf(b[z]) >= 0) {
                            for (i; i < a.length; i++) {
                                a[i] = a[i + 1];
                            }
                            a.pop();
                        }
                    }
                } else {
                    while (i = a.indexOf(b) >= 0) {
                        for (i; i < a.length; i++) {
                            a[i] = a[i + 1];
                        }
                        a.pop();
                    }
                }
            }
        }
    };
    Query.copy = function (b) {
        var value, a = {};
        for (var id in b) {
            value = b[id];
            if (typeof value == 'object') {
                a[id] = {};
                Query.copy(a[id], value);
            } else {
                a[id] = value;
            }
        }
        return a;
    };
    Query.update = function (a, b) {
        var id, parts, o;
        if (Object.keys(b.obj).length > 0) {
            id = a._id;
            a = Query.copy(b.obj);
            a._id = id;
        }
        vw.warning('OBJ2:', a, b);
        for (var id2 in b) {
            if (id2.startsWith('$')) {
                o = b[id2];
                for (var id in o) {
                    parts = Query.analizeParts(id.split('.'));
                    Query.ucompare(id2, a, parts, o[id]);
                }
            }
        }
        vw.warning('OBJ3:', a);
        return a;
    };
    Query.processForUpdate = function (obj) {
        var o = {}, set, a = {};
        for (var id in obj) {
            if (id.startsWith('$'))
                a[id] = obj[id];
            else
                o[id] = obj[id];
        }
        a.obj = o;
        return a;
    };
    Query.createProjectionMap = function (proj) {
        return function (a) {
            Query.projection(a, proj.projection, proj.mode);
            return a;
        };
    };
    Query.projection = function (value, projection, mode, prefix) {
        if (prefix === undefined) {
            prefix = '';
        }
        var val, elim, l;
        for (var id in value) {
            val = value[id];
            elim = false;
            n = prefix + (prefix ? '.' : '') + id;
            if (mode == 0) {
                l = projection[n];
                if (l == 0) {
                    delete value[id];
                    elim = true;
                }
            } else if (mode == 1) {
                l = projection[n];
                if (n == '_id' && l == 0 || n != '_id' && l == undefined) {
                    delete value[id];
                    elim = true;
                }
            }
            if (!elim) {
                if (val instanceof Array) {
                    {
                        var o;
                        var $_iterator4 = regeneratorRuntime.values(val), $_normal4 = false, $_err4;
                        try {
                            while (true) {
                                var $_step4 = $_iterator4.next();
                                if ($_step4.done) {
                                    $_normal4 = true;
                                    break;
                                }
                                {
                                    o = $_step4.value;
                                    if (typeof o == 'object')
                                        Query.projection(o, projection, mode, n);
                                }
                            }
                        } catch (e) {
                            $_normal4 = false;
                            $_err4 = e;
                        }
                        try {
                            if (!$_normal4 && $_iterator4['return']) {
                                $_iterator4['return']();
                            }
                        } catch (e) {
                        }
                        if ($_err4) {
                            throw $_err4;
                        }
                    }
                } else if (typeof val == 'object') {
                    Query.projection(val, projection, mode, n);
                }
            }
        }
    };
    Query.compare = function (a, b) {
        val = true;
        var o, p;
        if (a instanceof Array && b) {
            p = {};
            for (var id in b) {
                p[id] = b[id];
            }
            delete p.$exists;
            b = p;
        }
        if (a instanceof Array && b instanceof Array) {
            if (b.length != a.length)
                return false;
            for (var i = 0; i < a.length; i++) {
                if (!this.compare(a[i], b[i]))
                    return false;
            }
            return true;
        }
        if (a instanceof Array) {
            {
                var c;
                var $_iterator1 = regeneratorRuntime.values(a), $_normal1 = false, $_err1;
                try {
                    while (true) {
                        var $_step1 = $_iterator1.next();
                        if ($_step1.done) {
                            $_normal1 = true;
                            break;
                        }
                        {
                            c = $_step1.value;
                            if (this.compare(c, b))
                                return true;
                        }
                    }
                } catch (e) {
                    $_normal1 = false;
                    $_err1 = e;
                }
                try {
                    if (!$_normal1 && $_iterator1['return']) {
                        $_iterator1['return']();
                    }
                } catch (e) {
                }
                if ($_err1) {
                    throw $_err1;
                }
            }
            return false;
        }
        if (b instanceof RegExp) {
            return b.test(a ? a.toString() : null);
        }
        if (typeof b == 'object') {
            for (var id in b) {
                if (id == '$lt')
                    val = a < b[id];
                else if (id == '$lte')
                    val = a <= b[id];
                else if (id == '$gt')
                    val = a > b[id];
                else if (id == '$gte')
                    val = a >= b[id];
                else if (id == '$in')
                    val = Query.compare(b[id], a);
                else if (id == '$nin')
                    val = !Query.compare(b[id], a);
                else if (id == '$ne')
                    val = a != b[id];
                else if (id == '$exists')
                    val = (a !== null && a !== undefined) == b[id];
                else if (id.startsWith('$'))
                    throw new core.System.Exception('No se pudo reconocer el operador especial ' + id);
                else {
                    o = b[id];
                    val = Query.compare(a ? a[id] : null, o);
                }
                if (!val)
                    break;
            }
        } else {
            if (typeof a == 'string' && typeof b == 'string')
                val = a.toUpperCase() == b.toUpperCase();
            else
                val = a == b;
        }
        return val;
    };
    Query.prototype.analize = function (conditions, index) {
        if (index === undefined) {
            index = 0;
        }
        var values = this.values, code = [], parts, str, y;
        code.push('a' + index + ': function(self){ ');
        if (this.$update || this.$remove)
            code.push('self=self.data;');
        for (var id in conditions) {
            val = conditions[id];
            if (val) {
                values.push(val);
                if (id == '$or') {
                    str = 'v=';
                    y = 0;
                    {
                        var query;
                        var $_iterator5 = regeneratorRuntime.values(val), $_normal5 = false, $_err5;
                        try {
                            while (true) {
                                var $_step5 = $_iterator5.next();
                                if ($_step5.done) {
                                    $_normal5 = true;
                                    break;
                                }
                                {
                                    query = $_step5.value;
                                    index++;
                                    this.analize(query, index + y + 1);
                                    str += (y > 0 ? '||' : '') + 'q.a' + (index + y + 1) + '(self)';
                                    y++;
                                }
                            }
                        } catch (e) {
                            $_normal5 = false;
                            $_err5 = e;
                        }
                        try {
                            if (!$_normal5 && $_iterator5['return']) {
                                $_iterator5['return']();
                            }
                        } catch (e) {
                        }
                        if ($_err5) {
                            throw $_err5;
                        }
                    }
                    code.push(str);
                    code.push('if(!v) return false');
                } else if (id == '$and') {
                    str = 'v=';
                    y = 0;
                    {
                        var query;
                        var $_iterator6 = regeneratorRuntime.values(val), $_normal6 = false, $_err6;
                        try {
                            while (true) {
                                var $_step6 = $_iterator6.next();
                                if ($_step6.done) {
                                    $_normal6 = true;
                                    break;
                                }
                                {
                                    query = $_step6.value;
                                    this.analize(query, index + y + 1);
                                    str += (y > 0 ? '&&' : '') + 'q.a' + (index + y + 1) + '(self)';
                                    y++;
                                }
                            }
                        } catch (e) {
                            $_normal6 = false;
                            $_err6 = e;
                        }
                        try {
                            if (!$_normal6 && $_iterator6['return']) {
                                $_iterator6['return']();
                            }
                        } catch (e) {
                        }
                        if ($_err6) {
                            throw $_err6;
                        }
                    }
                    code.push(str);
                    code.push('if(!v) return false');
                } else if (id == '$not') {
                    this.analize(val, index + 1);
                    code.push('v= !q.a' + (index + 1) + '(self)');
                    code.push('if(!v) return false');
                } else {
                    parts = id.split('.');
                    code.push('a= self');
                    if (parts.length == 1) {
                        for (var i = 0; i < parts.length; i++) {
                            code.push('a= a?a[' + JSON.stringify(parts[i]) + ']:null');
                        }
                        code.push('c=values[' + (values.length - 1) + ']');
                        code.push('v= Query.compare(a,c)');
                    } else {
                        code.push('c=values[' + (values.length - 1) + ']');
                        values.push(this.analizeParts(parts));
                        code.push('v= Query.gcompare(a, values[' + values.length - 1 + '], c)');
                    }
                    code.push('if(!v) return false');
                }
            }
        }
        code.push('return true');
        code.push('}');
        this.funcs.push(code);
    };
    Query.prototype.analizeSort = function (condition) {
        var code = [], parts, val;
        code.push('s0: function(y,z){');
        for (var id in condition) {
            parts = id.split('.');
            val = condition[id];
            for (var i = 0; i < parts.length; i++) {
                if (i == 0)
                    code.push('a= y?y.' + parts[i] + ':null');
                else
                    code.push('a= a?a.' + parts[i] + ':null');
            }
            for (var i = 0; i < parts.length; i++) {
                if (i == 0)
                    code.push('b= z?z.' + parts[i] + ':null');
                else
                    code.push('b= b?b.' + parts[i] + ':null');
            }
            code.push('if(a>b) return ' + (val > 0 ? 1 : -1));
            code.push('if(a<b) return ' + (val > 0 ? -1 : 1));
        }
        code.push('return 0');
        code.push('}');
        this.funcs.push(code);
    };
    Query.prototype.find = function (condition, projection) {
        if (condition && Object.keys(condition).length > 0) {
            this.analize(condition);
        }
        if (projection && Object.keys(projection).length > 0) {
            this.analizeProjection(projection);
        }
    };
    Query.prototype.update = function (condition, update, options) {
        this.$update = update;
        this.$updateOptions = options;
        if (condition && Object.keys(condition).length > 0) {
            this.analize(condition);
        }
    };
    Query.prototype.remove = function (condition, options) {
        this.$remove = true;
        this.$updateOptions = options;
        if (condition && Object.keys(condition).length > 0) {
            this.analize(condition);
        }
    };
    Query.prototype.analizeProjection = function (projection) {
        var val, mode, idmode;
        for (var id in projection) {
            val = projection[id];
            if (val != 0 && val != 1)
                throw new core.System.Exception('La proyección no es válida');
            if (mode != undefined && mode != val)
                throw new core.System.Exception('La proyección no es válida. No se puede usar modo eliminar y modo agregar de manera simultánea');
            if (id != '_id')
                mode = val | 0;
            else
                idmode = val | 0;
        }
        if (mode == undefined)
            mode = idmode;
        this.$projection = {
            mode: mode,
            projection: projection
        };
    };
    Query.prototype.sort = function (condition) {
        if (Object.keys(condition).length > 0) {
            this.analizeSort(condition);
            this.$sorting = true;
        }
    };
    Query.prototype.count = function () {
        return this.execute({ count: true });
    };
    Query.prototype.execute = function callee$0$0($_ref0) {
        var count, sto, update, remove, data, dataLength, oSkip, skip, l, analytics, i;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    count = $_ref0 ? $_ref0.count : undefined;
                    sto = this.table.$storage;
                    data = [], dataLength = 0;
                    oSkip = this.$skip | 0;
                    skip = this.$sorting ? 0 : oSkip;
                    update = this.$update;
                    remove = this.$remove;
                    analytics = !!update || !!remove;
                    if (update) {
                        update = Query.processForUpdate(update);
                        if (this.$updateOptions && !this.$updateOptions.multi)
                            this.$limit = 1;
                    }
                    if (this.funcs.length > 0) {
                        this.generateCode();
                    }
                    context$1$0.next = 12;
                    return regeneratorRuntime.awrap(sto.readAndSplit({
                        analytics: analytics,
                        ondata: function (self$0) {
                            return function (rows) {
                                if (self$0.q && self$0.q.a0)
                                    rows = rows.filter(self$0.q.a0);
                                var maxLength = rows.length;
                                if (!self$0.$sorting && self$0.$limit) {
                                    maxLength = Math.min(self$0.$limit - dataLength, maxLength);
                                }
                                if (count) {
                                    dataLength += maxLength;
                                } else {
                                    for (var i = skip; i < maxLength; i++) {
                                        data.push(rows[i]);
                                        dataLength++;
                                    }
                                    if (skip > 0)
                                        skip = Math.max(0, skip - maxLength);
                                }
                                if (self$0.$sorting) {
                                    if (maxLength > 0)
                                        data.sort(self$0.q.s0);
                                    if (self$0.$limit && data.length > self$0.$limit) {
                                        l = data.length - self$0.$limit;
                                        for (var i = 0; i < l; i++) {
                                            data.pop();
                                        }
                                    }
                                }
                                if (!self$0.$sorting && data.length > self$0.$limit) {
                                    return false;
                                }
                            };
                        }(this),
                        groupcount: this.$sorting ? 50 : this.$limit || 30
                    }));
                case 12:
                    if (!count) {
                        context$1$0.next = 14;
                        break;
                    }
                    return context$1$0.abrupt('return', dataLength);
                case 14:
                    if (oSkip > 0 && this.$sorting) {
                        for (i = 0; i < Math.min(data.length, oSkip); i++) {
                            data.shift();
                        }
                    }
                    if (!this.$update) {
                        context$1$0.next = 29;
                        break;
                    }
                    count = data.length;
                    if (!(data.length == 0 && this.$updateOptions && this.$updateOptions.upsert)) {
                        context$1$0.next = 23;
                        break;
                    }
                    data = Query.update({ _id: Kawasa.Util.uniqueId() }, update);
                    sto.insert(data);
                    count++;
                    context$1$0.next = 26;
                    break;
                case 23:
                    if (!(data.length > 0)) {
                        context$1$0.next = 26;
                        break;
                    }
                    context$1$0.next = 26;
                    return regeneratorRuntime.awrap(this._update(sto, data, update));
                case 26:
                    return context$1$0.abrupt('return', count);
                case 29:
                    if (!this.$remove) {
                        context$1$0.next = 37;
                        break;
                    }
                    count = data.length;
                    if (!(data.length > 0)) {
                        context$1$0.next = 34;
                        break;
                    }
                    context$1$0.next = 34;
                    return regeneratorRuntime.awrap(this._remove(sto, data));
                case 34:
                    return context$1$0.abrupt('return', count);
                case 37:
                    if (this.$projection) {
                        data = data.map(Query.createProjectionMap(this.$projection));
                    }
                case 38:
                    return context$1$0.abrupt('return', this.$one ? data[0] : data);
                case 39:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Query.prototype._update = function (sto, data, update) {
        var d;
        vw.log(update);
        for (var i = 0; i < data.length; i++) {
            d = data[i];
            d.data = Query.update(d.data, update);
        }
        return sto.insert(data, { edit: true });
    };
    Query.prototype._remove = function callee$0$0(sto, data) {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    return context$1$0.abrupt('return', sto.remove(data));
                case 1:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Query.prototype.limit = function (limit) {
        this.$limit = limit;
        return this;
    };
    Query.prototype.generateCode = function () {
        var str = this.createCode();
        var q;
        var values = this.values;
        eval(str);
        this.q = q;
    };
    Query.prototype.createCode = function () {
        var str = 'q={';
        var y = 0;
        {
            var code;
            var $_iterator0 = regeneratorRuntime.values(this.funcs), $_normal0 = false, $_err0;
            try {
                while (true) {
                    var $_step0 = $_iterator0.next();
                    if ($_step0.done) {
                        $_normal0 = true;
                        break;
                    }
                    {
                        code = $_step0.value;
                        str += code.join('\n');
                        if (y != this.funcs.length - 1)
                            str += ',';
                        y++;
                    }
                }
            } catch (e) {
                $_normal0 = false;
                $_err0 = e;
            }
            try {
                if (!$_normal0 && $_iterator0['return']) {
                    $_iterator0['return']();
                }
            } catch (e) {
            }
            if ($_err0) {
                throw $_err0;
            }
        }
        str += '}';
        return str;
    };
}
exports.default = Query;