var $mod$2 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var Kawasa = core.org.voxsoftware.Database.Kawasa;
var space = new Buffer(50);
space.fill(0);
{
    var Table = function Table() {
        Table.$constructor ? Table.$constructor.apply(this, arguments) : Table.$superClass && Table.$superClass.apply(this, arguments);
    };
    Table.$constructor = function (stream, table) {
        this.$stream = stream;
        this.$pending = [];
        this.$table = table;
        this.createDocument();
        this.readBuffer = new Buffer(9 * 1024);
    };
    Table.prototype.queue = function (action) {
        action.task = new core.VW.Task();
        this.$pending.push(action);
        if (!this.$working)
            this.doQueue();
        return action.task;
    };
    Table.prototype.doQueue = function callee$0$0() {
        var pending, er, action, $_iterator9, $_normal9, $_err9, $_step9;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$working = true;
                    pending = this.$pending;
                    this.$pending = [];
                    context$1$0.prev = 3;
                    $_iterator9 = regeneratorRuntime.values(pending), $_normal9 = false;
                    context$1$0.prev = 5;
                case 6:
                    if (!true) {
                        context$1$0.next = 24;
                        break;
                    }
                    $_step9 = $_iterator9.next();
                    if (!$_step9.done) {
                        context$1$0.next = 11;
                        break;
                    }
                    $_normal9 = true;
                    return context$1$0.abrupt('break', 24);
                case 11:
                    action = $_step9.value;
                    context$1$0.prev = 12;
                    context$1$0.next = 15;
                    return regeneratorRuntime.awrap(this.realize(action));
                case 15:
                    action.task.result = context$1$0.sent;
                    context$1$0.next = 21;
                    break;
                case 18:
                    context$1$0.prev = 18;
                    context$1$0.t0 = context$1$0['catch'](12);
                    action.task.exception = context$1$0.t0;
                case 21:
                    action.task.finish();
                    context$1$0.next = 6;
                    break;
                case 24:
                    context$1$0.next = 30;
                    break;
                case 26:
                    context$1$0.prev = 26;
                    context$1$0.t1 = context$1$0['catch'](5);
                    $_normal9 = false;
                    $_err9 = context$1$0.t1;
                case 30:
                    try {
                        if (!$_normal9 && $_iterator9['return']) {
                            $_iterator9['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err9) {
                        context$1$0.next = 33;
                        break;
                    }
                    throw $_err9;
                case 33:
                    context$1$0.next = 38;
                    break;
                case 35:
                    context$1$0.prev = 35;
                    context$1$0.t2 = context$1$0['catch'](3);
                    er = context$1$0.t2;
                case 38:
                    if (!(!er && this.$pending.length)) {
                        context$1$0.next = 41;
                        break;
                    }
                    this.doQueue();
                    return context$1$0.abrupt('return');
                case 41:
                    this.$working = false;
                    if (!er) {
                        context$1$0.next = 44;
                        break;
                    }
                    throw er;
                case 44:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this, [
            [
                3,
                35
            ],
            [
                5,
                26
            ],
            [
                12,
                18
            ]
        ]);
    };
    Table.prototype.waitQueue = function () {
        var tasks = this.$pending.map(function (x) {
            return x.task;
        });
        return core.VW.Task.waitMany(tasks);
    };
    Table.prototype.realize = function (action) {
        if (action.type == 'insert') {
            return this.insert(action.docs);
        } else if (action.type == 'remove') {
            action.query.remove(action.args[0], action.args[1]);
            return action.query.execute();
        } else if (action.type == 'update') {
            action.query.update(action.args[0], action.args[1], action.args[2]);
            return action.query.execute();
        } else if (action.type == 'countall') {
            return this.count();
        }
    };
    Table.prototype.createDocument = function () {
        this.$breader = new core.System.IO.BinaryReader(this.$stream);
        this.$bwriter = new core.System.IO.BinaryWriter(this.$stream);
        var str;
        this.$stream.position = 0;
        vw.log(this.$stream.length);
        if (this.$stream.length == 0) {
            this.$bwriter.writeString('kaw#');
            this.$start = this.$stream.position;
            this.$bwriter.writeUInt32(0);
            this.$bwriter.writeByte(10);
        } else {
            str = this.$breader.readString();
            if (str != 'kaw#')
                throw new core.System.Exception('El archivo de tabla tiene un formato no válido');
            this.$start = this.$stream.position;
        }
        this.$stream.position = this.$stream.length;
    };
    Table.prototype._añadirConteo = function callee$0$0(count) {
        var c;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$stream.position = this.$start;
                    context$1$0.next = 3;
                    return regeneratorRuntime.awrap(this.$breader.readUInt32Async());
                case 3:
                    context$1$0.t0 = context$1$0.sent;
                    context$1$0.t1 = count || 1;
                    c = context$1$0.t0 + context$1$0.t1;
                    this.$stream.position = this.$start;
                    context$1$0.next = 9;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUInt32Async(c));
                case 9:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.findAll = function callee$0$0() {
        var data;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    data = [];
                    context$1$0.next = 3;
                    return regeneratorRuntime.awrap(this.readAndSplit({
                        ondata: function (rows) {
                            for (var i = 0; i < rows.length; i++) {
                                data.push(rows[i]);
                            }
                        },
                        groupcount: 10000
                    }));
                case 3:
                    return context$1$0.abrupt('return', data);
                case 4:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.remove = function callee$0$0(positions) {
        var count, position, $_iterator10, $_normal10, $_err10, $_step10;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    count = 0;
                    $_iterator10 = regeneratorRuntime.values(positions), $_normal10 = false;
                    context$1$0.prev = 2;
                case 3:
                    if (!true) {
                        context$1$0.next = 16;
                        break;
                    }
                    $_step10 = $_iterator10.next();
                    if (!$_step10.done) {
                        context$1$0.next = 8;
                        break;
                    }
                    $_normal10 = true;
                    return context$1$0.abrupt('break', 16);
                case 8:
                    position = $_step10.value;
                    vw.warning('Removing ', position.start);
                    this.$stream.position = position.start + 1;
                    context$1$0.next = 13;
                    return regeneratorRuntime.awrap(this.$stream.writeByteAsync(199));
                case 13:
                    count--;
                    context$1$0.next = 3;
                    break;
                case 16:
                    context$1$0.next = 22;
                    break;
                case 18:
                    context$1$0.prev = 18;
                    context$1$0.t0 = context$1$0['catch'](2);
                    $_normal10 = false;
                    $_err10 = context$1$0.t0;
                case 22:
                    try {
                        if (!$_normal10 && $_iterator10['return']) {
                            $_iterator10['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err10) {
                        context$1$0.next = 25;
                        break;
                    }
                    throw $_err10;
                case 25:
                    context$1$0.next = 27;
                    return regeneratorRuntime.awrap(this._añadirConteo(count));
                case 27:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this, [[
                2,
                18
            ]]);
    };
    Table.prototype.readAndSplit = function callee$0$0($_ref4) {
        var ondata, groupcount, analytics, noData, search, lead, endSearch, str, d, buf, data, pos;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    ondata = $_ref4 ? $_ref4.ondata : undefined, groupcount = $_ref4 ? $_ref4.groupcount : undefined, analytics = $_ref4 ? $_ref4.analytics : undefined, noData = $_ref4 ? $_ref4.noData : undefined;
                    search = -1;
                    buf = new Buffer(2), data = [];
                    buf[0] = 10;
                    buf[1] = 200;
                    this.$stream.position = this.$start;
                case 6:
                    if (!true) {
                        context$1$0.next = 23;
                        break;
                    }
                    pos = this.$stream.position;
                    context$1$0.next = 10;
                    return regeneratorRuntime.awrap(this.readPortion());
                case 10:
                    if (!context$1$0.sent) {
                        context$1$0.next = 20;
                        break;
                    }
                    search = -1;
                    if (lead)
                        this.currentBuffer = Buffer.concat([
                            lead,
                            this.currentBuffer
                        ]);
                    while ((search = this.currentBuffer.indexOf(buf, search + 1)) >= 0) {
                        endSearch = this.currentBuffer.indexOf(10, search + 1);
                        if (endSearch > 0) {
                            str = this.currentBuffer.slice(search + 2, endSearch).toString();
                            if (analytics) {
                                d = noData ? null : JSON.parse(str);
                                data.push({
                                    data: d,
                                    portionstart: pos,
                                    start: pos + search,
                                    end: endSearch + pos
                                });
                            } else {
                                if (!noData)
                                    data.push(JSON.parse(str));
                            }
                        } else {
                            lead = new Buffer(this.currentBuffer.slice(search, this.currentBuffer.length));
                        }
                    }
                    if (!(data.length >= groupcount)) {
                        context$1$0.next = 18;
                        break;
                    }
                    if (ondata(data)) {
                        context$1$0.next = 17;
                        break;
                    }
                    return context$1$0.abrupt('break', 23);
                case 17:
                    data = [];
                case 18:
                    context$1$0.next = 21;
                    break;
                case 20:
                    return context$1$0.abrupt('break', 23);
                case 21:
                    context$1$0.next = 6;
                    break;
                case 23:
                    if (data.length > 0) {
                        ondata(data);
                    }
                case 24:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.readPortion = function callee$0$0() {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    context$1$0.next = 2;
                    return regeneratorRuntime.awrap(this.$stream.readAsync(this.readBuffer, 0, this.readBuffer.length));
                case 2:
                    this.readed = context$1$0.sent;
                    this.currentBuffer = this.readBuffer.slice(0, this.readed);
                    return context$1$0.abrupt('return', this.readed > 0);
                case 5:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.insert = function callee$0$0(document, $_ref5) {
        var edit, documents, positions, er, buffer, len, id, json, _id, slen, doc, after, $_iterator12, $_normal12, $_err12, $_step12, position, $_iterator11, $_normal11, $_err11, $_step11;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    edit = $_ref5 ? $_ref5.edit : undefined;
                    positions = [];
                    if (document instanceof Array) {
                        documents = document;
                    } else {
                        documents = [document];
                    }
                    slen = this.$stream.length;
                    this.$stream.position = slen;
                    context$1$0.prev = 5;
                    after = 2;
                    $_iterator12 = regeneratorRuntime.values(documents), $_normal12 = false;
                    context$1$0.prev = 8;
                case 9:
                    if (!true) {
                        context$1$0.next = 30;
                        break;
                    }
                    $_step12 = $_iterator12.next();
                    if (!$_step12.done) {
                        context$1$0.next = 14;
                        break;
                    }
                    $_normal12 = true;
                    return context$1$0.abrupt('break', 30);
                case 14:
                    document = $_step12.value;
                    if (edit) {
                        doc = document;
                        document = document.data;
                    }
                    if (!document._id || !edit) {
                        id = Kawasa.Util.uniqueId();
                        document._id = id;
                    }
                    json = core.safeJSON.stringify(document);
                    len = Buffer.byteLength(json);
                    if (edit && len + 2 < doc.end - doc.start) {
                        after = 4;
                    }
                    if (!buffer || buffer.length < len + after) {
                        buffer = new Buffer(len + after);
                    }
                    buffer.write(json, 1, len + 1);
                    buffer[len + 1] = 10;
                    if (after > 2) {
                        buffer[len + 2] = 199;
                        buffer[len + 3] = 10;
                    }
                    buffer[0] = 200;
                    if (edit) {
                        vw.info(doc.end, doc.start);
                        if (len + 2 <= doc.end - doc.start) {
                            this.$stream.position = doc.start + 1;
                        } else {
                            positions.push(doc.start + 1);
                            this.$stream.position = this.$stream.length;
                        }
                    }
                    context$1$0.next = 28;
                    return regeneratorRuntime.awrap(this.$bwriter.writeBytesAsync(buffer, len + after));
                case 28:
                    context$1$0.next = 9;
                    break;
                case 30:
                    context$1$0.next = 36;
                    break;
                case 32:
                    context$1$0.prev = 32;
                    context$1$0.t0 = context$1$0['catch'](8);
                    $_normal12 = false;
                    $_err12 = context$1$0.t0;
                case 36:
                    try {
                        if (!$_normal12 && $_iterator12['return']) {
                            $_iterator12['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err12) {
                        context$1$0.next = 39;
                        break;
                    }
                    throw $_err12;
                case 39:
                    context$1$0.next = 50;
                    break;
                case 41:
                    context$1$0.prev = 41;
                    context$1$0.t1 = context$1$0['catch'](5);
                    if (!this.$stream.setLengthAsync) {
                        context$1$0.next = 48;
                        break;
                    }
                    context$1$0.next = 46;
                    return regeneratorRuntime.awrap(this.$stream.setLengthAsync(slen));
                case 46:
                    context$1$0.next = 49;
                    break;
                case 48:
                    this.$stream.setLength(slen);
                case 49:
                    er = context$1$0.t1;
                case 50:
                    if (!er) {
                        context$1$0.next = 52;
                        break;
                    }
                    throw er;
                case 52:
                    len = this.$stream.length;
                    $_iterator11 = regeneratorRuntime.values(positions), $_normal11 = false;
                    context$1$0.prev = 54;
                case 55:
                    if (!true) {
                        context$1$0.next = 66;
                        break;
                    }
                    $_step11 = $_iterator11.next();
                    if (!$_step11.done) {
                        context$1$0.next = 60;
                        break;
                    }
                    $_normal11 = true;
                    return context$1$0.abrupt('break', 66);
                case 60:
                    position = $_step11.value;
                    this.$stream.position = position;
                    this.$stream.writeByte(199);
                    if (position + 1 >= len) {
                        this.$stream.writeByte(10);
                    }
                    context$1$0.next = 55;
                    break;
                case 66:
                    context$1$0.next = 72;
                    break;
                case 68:
                    context$1$0.prev = 68;
                    context$1$0.t2 = context$1$0['catch'](54);
                    $_normal11 = false;
                    $_err11 = context$1$0.t2;
                case 72:
                    try {
                        if (!$_normal11 && $_iterator11['return']) {
                            $_iterator11['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err11) {
                        context$1$0.next = 75;
                        break;
                    }
                    throw $_err11;
                case 75:
                    if (edit) {
                        context$1$0.next = 78;
                        break;
                    }
                    context$1$0.next = 78;
                    return regeneratorRuntime.awrap(this._añadirConteo(documents.length));
                case 78:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this, [
            [
                5,
                41
            ],
            [
                8,
                32
            ],
            [
                54,
                68
            ]
        ]);
    };
    Table.prototype.count = function callee$0$0() {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$stream.position = this.$start;
                    context$1$0.next = 3;
                    return regeneratorRuntime.awrap(this.$breader.readUInt32());
                case 3:
                    return context$1$0.abrupt('return', context$1$0.sent);
                case 4:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
}
exports.default = Table;