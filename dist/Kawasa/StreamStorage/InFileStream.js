var IO = core.System.IO;
var debug = false;
{
    var InFileStream = function InFileStream() {
        InFileStream.$constructor ? InFileStream.$constructor.apply(this, arguments) : InFileStream.$superClass && InFileStream.$superClass.apply(this, arguments);
    };
    InFileStream.prototype = Object.create(IO.Stream.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(InFileStream, IO.Stream) : InFileStream.__proto__ = IO.Stream;
    InFileStream.prototype.constructor = InFileStream;
    InFileStream.$super = IO.Stream.prototype;
    InFileStream.$superClass = IO.Stream;
    InFileStream.$constructor = function (dstream, fileinfo, datablock) {
        this.$dstream = dstream;
        this.$fileinfo = fileinfo;
        this.$block = datablock;
        this.$blockingMode = true;
        this.$pos = 0;
    };
    InFileStream.prototype.__defineGetter__('_stream', function () {
        if (this.$stream)
            return this.$stream;
        else
            return this.$dstream.$stream;
    });
    InFileStream.prototype.__defineGetter__('_breader', function () {
        if (this.$breader)
            return this.$breader;
        else
            return this.$dstream.$breader;
    });
    InFileStream.prototype.__defineGetter__('_bwriter', function () {
        if (this.$bwriter)
            return this.$bwriter;
        else
            return this.$dstream.$bwriter;
    });
    InFileStream.prototype.__defineGetter__('length', function () {
        this._stream.position = this.$fileinfo.final + this.$fileinfo.blockstart - 14;
        var c = this._breader.readUIntSafe(true);
        return c;
    });
    InFileStream.prototype.getLengthAsync = function () {
        this._stream.position = this.$fileinfo.final + this.$fileinfo.blockstart - 14;
        return this._breader.readUIntSafeAsync(true);
    };
    InFileStream.prototype.__defineGetter__('position', function () {
        return this.$pos;
    });
    InFileStream.prototype.setLength = function () {
        throw new core.System.NotImplementedException();
    };
    InFileStream.prototype.setLengthAsync = function (num) {
        this._stream.position = this.$fileinfo.final + this.$fileinfo.blockstart - 14;
        return this._bwriter.writeUIntSafeAsync(num, true);
    };
    InFileStream.prototype.__defineSetter__('position', function (pos) {
        if (pos > this.length || pos < 0) {
            throw new core.System.Exception('La posición no es válida');
        }
        this.$pos = pos;
    });
    InFileStream.prototype.ubicate = function callee$0$0(len, ev, pos, create) {
        var block, cblock, index, i;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    if (ev === undefined) {
                        ev = {};
                    }
                    if (create === undefined) {
                        create = false;
                    }
                    if (!(pos == undefined)) {
                        context$1$0.next = 11;
                        break;
                    }
                    debug && vw.warning('Exscribiendo len:', len);
                    context$1$0.next = 6;
                    return regeneratorRuntime.awrap(this.ubicate(undefined, ev, this.$pos, create));
                case 6:
                    block1 = context$1$0.sent;
                    context$1$0.next = 9;
                    return regeneratorRuntime.awrap(this.ubicate(undefined, ev, this.$pos + len, create));
                case 9:
                    block2 = context$1$0.sent;
                    return context$1$0.abrupt('return', [
                        block1,
                        block2
                    ]);
                case 11:
                    block = this.$block;
                    index = 0;
                case 13:
                    if (!block) {
                        context$1$0.next = 34;
                        break;
                    }
                    block.realsize = block.size - 8;
                    if (!(block.realsize + index >= pos)) {
                        context$1$0.next = 19;
                        break;
                    }
                    i = pos - index;
                    cblock = block;
                    return context$1$0.abrupt('break', 34);
                case 19:
                    index += block.realsize;
                    if (!(!block.next && !ev.parsed)) {
                        context$1$0.next = 24;
                        break;
                    }
                    context$1$0.next = 23;
                    return regeneratorRuntime.awrap(this.$dstream.getFileBlock({
                        getFirst: true,
                        fromBlock: block
                    }));
                case 23:
                    ev.parsed = true;
                case 24:
                    if (!(!block.next && create)) {
                        context$1$0.next = 31;
                        break;
                    }
                    context$1$0.next = 27;
                    return regeneratorRuntime.awrap(this.$dstream.createDataBlock(block));
                case 27:
                    block = context$1$0.sent;
                    debug && vw.warning('Escribiento otro bloque necesario para escribir');
                    context$1$0.next = 32;
                    break;
                case 31:
                    block = block.next;
                case 32:
                    context$1$0.next = 13;
                    break;
                case 34:
                    return context$1$0.abrupt('return', {
                        i: i,
                        block: cblock
                    });
                case 35:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    InFileStream.prototype.ubicateSync = function (len, ev, pos, create) {
        if (ev === undefined) {
            ev = {};
        }
        if (create === undefined) {
            create = false;
        }
        if (pos == undefined) {
            debug && vw.warning('Exscribiendo len:', len);
            block1 = this.ubicateSync(undefined, ev, this.$pos, create);
            block2 = this.ubicateSync(undefined, ev, this.$pos + len, create);
            return [
                block1,
                block2
            ];
        }
        var block = this.$block, cblock;
        var index = 0, i;
        while (block) {
            block.realsize = block.size - 8;
            if (block.realsize + index >= pos) {
                i = pos - index;
                cblock = block;
                break;
            }
            index += block.realsize;
            if (!block.next && !ev.parsed) {
                this.$dstream.getFileBlockSync({
                    getFirst: true,
                    fromBlock: block
                });
                ev.parsed = true;
            }
            if (!block.next && create) {
                block = this.$dstream.createDataBlockSync(block);
                debug && vw.warning('Escribiento otro bloque necesario para escribir');
            } else
                block = block.next;
        }
        return {
            i: i,
            block: cblock
        };
    };
    InFileStream.prototype.readAsync = function callee$0$0(bytes, offset, count) {
        var blocks, readed, rd, block, initial;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    if (!(this.$canread === false)) {
                        context$1$0.next = 2;
                        break;
                    }
                    throw new core.System.Exception('No tiene permisos de lectura sobre este flujo');
                case 2:
                    context$1$0.next = 4;
                    return regeneratorRuntime.awrap(this.ubicate(count));
                case 4:
                    blocks = context$1$0.sent;
                    readed = 0;
                    block = blocks[0].block;
                    initial = blocks[0].i;
                case 8:
                    if (!block) {
                        context$1$0.next = 23;
                        break;
                    }
                    this._stream.position = block.start + initial;
                    context$1$0.next = 12;
                    return regeneratorRuntime.awrap(this._stream.readAsync(bytes, offset, Math.min(block.realsize - initial, count)));
                case 12:
                    rd = context$1$0.sent;
                    initial = 0;
                    offset += rd;
                    readed += rd;
                    count -= rd;
                    this.$pos += rd;
                    if (!(count <= 0)) {
                        context$1$0.next = 20;
                        break;
                    }
                    return context$1$0.abrupt('break', 23);
                case 20:
                    block = block.next;
                    context$1$0.next = 8;
                    break;
                case 23:
                    return context$1$0.abrupt('return', readed);
                case 24:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    InFileStream.prototype.read = function (bytes, offset, count) {
        if (this.$canread === false)
            throw new core.System.Exception('No tiene permisos de lectura sobre este flujo');
        var blocks = this.ubicateSync(count);
        var readed = 0, rd;
        var block = blocks[0].block;
        var initial = blocks[0].i;
        while (block) {
            this._stream.position = block.start + initial;
            rd = this._stream.read(bytes, offset, Math.min(block.realsize - initial, count));
            initial = 0;
            offset += rd;
            readed += rd;
            count -= rd;
            this.$pos += rd;
            if (count <= 0)
                break;
            block = block.next;
        }
        return readed;
    };
    InFileStream.prototype.writeAsync = function callee$0$0(bytes, offset, count) {
        var blocks, readed, rd, block, initial, len;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    if (!(this.$canwrite === false)) {
                        context$1$0.next = 2;
                        break;
                    }
                    throw new core.System.Exception('No tiene permisos de escritura sobre este flujo');
                case 2:
                    context$1$0.next = 4;
                    return regeneratorRuntime.awrap(this.ubicate(count, undefined, undefined, true));
                case 4:
                    blocks = context$1$0.sent;
                    readed = 0;
                    block = blocks[0].block;
                    initial = blocks[0].i;
                case 8:
                    if (!block) {
                        context$1$0.next = 23;
                        break;
                    }
                    this._stream.position = block.start + initial;
                    context$1$0.next = 12;
                    return regeneratorRuntime.awrap(this._stream.writeAsync(bytes, offset, Math.min(block.realsize - initial, count)));
                case 12:
                    rd = context$1$0.sent;
                    initial = 0;
                    offset += rd;
                    readed += rd;
                    count -= rd;
                    this.$pos += rd;
                    if (!(count <= 0)) {
                        context$1$0.next = 20;
                        break;
                    }
                    return context$1$0.abrupt('break', 23);
                case 20:
                    block = block.next;
                    context$1$0.next = 8;
                    break;
                case 23:
                    context$1$0.next = 25;
                    return regeneratorRuntime.awrap(this.getLengthAsync());
                case 25:
                    len = context$1$0.sent;
                    this._stream.position = this.$fileinfo.final + this.$fileinfo.blockstart - 14;
                    context$1$0.next = 29;
                    return regeneratorRuntime.awrap(this._bwriter.writeUIntSafeAsync(len + readed, true));
                case 29:
                    return context$1$0.abrupt('return', readed);
                case 30:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    InFileStream.prototype.write = function (bytes, offset, count) {
        if (this.$canwrite === false)
            throw new core.System.Exception('No tiene permisos de escritura sobre este flujo');
        var blocks = this.ubicateSync(count, undefined, undefined, true);
        var readed = 0, rd;
        var block = blocks[0].block;
        var initial = blocks[0].i;
        while (block) {
            this._stream.position = block.start + initial;
            rd = this._stream.write(bytes, offset, Math.min(block.realsize - initial, count));
            initial = 0;
            offset += rd;
            readed += rd;
            count -= rd;
            this.$pos += rd;
            if (count <= 0)
                break;
            block = block.next;
        }
        var len = this.length;
        this._stream.position = this.$fileinfo.final + this.$fileinfo.blockstart - 14;
        this._bwriter.writeUIntSafe(len + readed, true);
        return readed;
    };
}
exports.default = InFileStream;