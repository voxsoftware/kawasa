var Kawasa = core.org.voxsoftware.Database.Kawasa;
var Fs = core.System.IO.Fs;
{
    var Database = function Database() {
        Database.$constructor ? Database.$constructor.apply(this, arguments) : Database.$superClass && Database.$superClass.apply(this, arguments);
    };
    Database.$constructor = function (folder) {
        this.path = folder;
        if (!Fs.sync.exists(folder))
            Fs.sync.mkdir(folder);
    };
    Database.prototype.table = function (name) {
        var table = new Kawasa.Table(name);
        table.db = this;
        return table;
    };
}
exports.default = Database;