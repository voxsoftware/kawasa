var $mod$4 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var Kawasa = core.org.voxsoftware.Database.Kawasa;
{
    var CompactedTable = function CompactedTable() {
        CompactedTable.$constructor ? CompactedTable.$constructor.apply(this, arguments) : CompactedTable.$superClass && CompactedTable.$superClass.apply(this, arguments);
    };
    CompactedTable.prototype = Object.create(Kawasa.Table.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(CompactedTable, Kawasa.Table) : CompactedTable.__proto__ = Kawasa.Table;
    CompactedTable.prototype.constructor = CompactedTable;
    CompactedTable.$super = Kawasa.Table.prototype;
    CompactedTable.$superClass = Kawasa.Table;
    CompactedTable.$constructor = function (name) {
        this.name = name;
    };
    CompactedTable.prototype.__defineGetter__('db', function () {
        return this._db;
    });
    CompactedTable.prototype.__defineSetter__('db', function (db) {
        this._db = db;
        this.fileData = this.name;
        this.fileIndex = this.name + '.idx';
    });
    CompactedTable.prototype.start = function callee$0$0() {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    context$1$0.next = 2;
                    return regeneratorRuntime.awrap(this._db.dStream.open(this.fileData, core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite));
                case 2:
                    this.$stream = context$1$0.sent;
                    this.$storage = new Kawasa.StreamStorage.Table(this.$stream, this);
                case 4:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
}
exports.default = CompactedTable;