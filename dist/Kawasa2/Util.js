{
    var Util = function Util() {
        Util.$constructor ? Util.$constructor.apply(this, arguments) : Util.$superClass && Util.$superClass.apply(this, arguments);
    };
    Util.uniqueId = function () {
        var d = Date.now();
        var last = Util.last || d;
        d = Util.last = d > last ? d : last + 1;
        return Util.prefix + d.toString(36);
    };
}
if (!Util.prefix) {
    Util.prefix = parseFloat((Math.random() * Date.now()).toFixed(1)).toString(32);
    Util.prefix = process.pid + Util.prefix;
    Util.prefix = Util.prefix.replace('.', '').substring(0, 14);
    if (Util.prefix.length < 14) {
        for (var i = Util.prefix.length; i < 14; i++) {
            Util.prefix += 'w';
        }
    }
}
exports.default = Util;