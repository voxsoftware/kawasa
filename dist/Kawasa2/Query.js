var Kawasa = core.org.voxsoftware.Database.Kawasa;
var $mod$5 = core.VW.Ecma2015.Utils.module(require('path'));
{
    var Query = function Query() {
        Query.$constructor ? Query.$constructor.apply(this, arguments) : Query.$superClass && Query.$superClass.apply(this, arguments);
    };
    Query.$constructor = function () {
        this.clear();
    };
    Query.prototype.clear = function () {
        this.funcs = [];
        this.values = [];
        this.q = null;
    };
    Query.analizeParts = function (parts) {
        var p = [];
        for (var i = 0; i < parts.length; i++) {
            p.push({
                text: parts[i],
                number: /^\d+$/.test(parts[i])
            });
        }
        return p;
    };
    Query.prototype.analizeParts = function (a) {
        return Query.analizeParts(a);
    };
    Query.gcompare = function (a, parts, b, indexStart) {
        if (indexStart === undefined) {
            indexStart = 0;
        }
        var part;
        for (var i = indexStart; i < parts.length; i++) {
            part = parts[i];
            a = a[part.text];
            if (a && i < parts.length - 1 && !parts[i + 1].number & a instanceof Array) {
                {
                    var c;
                    var $_iterator15 = regeneratorRuntime.values(a), $_normal15 = false, $_err15;
                    try {
                        while (true) {
                            var $_step15 = $_iterator15.next();
                            if ($_step15.done) {
                                $_normal15 = true;
                                break;
                            }
                            {
                                c = $_step15.value;
                                if (Query.gcompare(c, parts, b, i + 1))
                                    return true;
                            }
                        }
                    } catch (e) {
                        $_normal15 = false;
                        $_err15 = e;
                    }
                    try {
                        if (!$_normal15 && $_iterator15['return']) {
                            $_iterator15['return']();
                        }
                    } catch (e) {
                    }
                    if ($_err15) {
                        throw $_err15;
                    }
                }
                return false;
            }
            if (!a)
                break;
        }
        return Query.compare(a, b);
    };
    Query.ucompare = function (mode, a, parts, b, indexStart) {
        if (indexStart === undefined) {
            indexStart = 0;
        }
        var part, i, slice;
        vw.log(parts);
        for (var i = indexStart; i < parts.length - 1; i++) {
            part = parts[i];
            a = a[part.text] || {};
            if (a && i < parts.length - 1 && !parts[i + 1].number & a instanceof Array) {
                {
                    var c;
                    var $_iterator16 = regeneratorRuntime.values(a), $_normal16 = false, $_err16;
                    try {
                        while (true) {
                            var $_step16 = $_iterator16.next();
                            if ($_step16.done) {
                                $_normal16 = true;
                                break;
                            }
                            {
                                c = $_step16.value;
                                Query.ucompare(mode, c, parts, b, i + 1);
                            }
                        }
                    } catch (e) {
                        $_normal16 = false;
                        $_err16 = e;
                    }
                    try {
                        if (!$_normal16 && $_iterator16['return']) {
                            $_iterator16['return']();
                        }
                    } catch (e) {
                    }
                    if ($_err16) {
                        throw $_err16;
                    }
                }
                return;
            }
            if (!a)
                break;
        }
        if (mode == '$set')
            a[parts[parts.length - 1].text] = b;
        else if (mode == '$unset')
            delete a[parts[parts.length - 1].text];
        else if (mode == '$inc')
            a[parts[parts.length - 1].text] = a[parts[parts.length - 1].text] + b;
        else if (mode == '$max')
            a[parts[parts.length - 1].text] = Math.max(a[parts[parts.length - 1].text], b);
        else if (mode == '$min')
            a[parts[parts.length - 1].text] = Math.min(a[parts[parts.length - 1].text], b);
        else if (mode == '$push') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array) {
                if (typeof b == 'object' && b.$slice) {
                    slice = b.$slice;
                    if (!b.$each) {
                        throw new core.System.Exception('Debe especificar la propiedad each junto a slice');
                    }
                    b.$each = b.$each.slice(0, Math.min(0, slice - a.length));
                }
                if (typeof b == 'object' && b.$each) {
                    for (var z = 0; z < b.$each.lenght; z++) {
                        a.push(b.$each[z]);
                    }
                } else {
                    a.push(b);
                }
                if (a.length > slice) {
                    a = a.slice(0, slice);
                }
            }
        } else if (mode == '$addToSet') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array && a.indexOf(b) < 0)
                a.push(b);
        } else if (mode == '$pop') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array) {
                if (b > 0)
                    a.pop();
                else if (b < 0)
                    a.shift();
            }
        } else if (mode == '$pull') {
            a = a[parts[parts.length - 1].text];
            if (a instanceof Array) {
                if (typeof b == 'object' && b.$in) {
                    for (var z = 0; z < b.$in.lenght; z++) {
                        while (i = a.indexOf(b[z]) >= 0) {
                            for (i; i < a.length; i++) {
                                a[i] = a[i + 1];
                            }
                            a.pop();
                        }
                    }
                } else {
                    while (i = a.indexOf(b) >= 0) {
                        for (i; i < a.length; i++) {
                            a[i] = a[i + 1];
                        }
                        a.pop();
                    }
                }
            }
        }
    };
    Query.copy = function (b) {
        var value, a = {};
        for (var id in b) {
            value = b[id];
            if (typeof value == 'object') {
                a[id] = {};
                Query.copy(a[id], value);
            } else {
                a[id] = value;
            }
        }
        return a;
    };
    Query.update = function (a, b) {
        var id, parts, o;
        if (Object.keys(b.obj).length > 0) {
            id = a._id;
            a = Query.copy(b.obj);
            a._id = id;
        }
        for (var id2 in b) {
            if (id2.startsWith('$')) {
                o = b[id2];
                for (var id in o) {
                    parts = Query.analizeParts(id.split('.'));
                    Query.ucompare(id2, a, parts, o[id]);
                }
            }
        }
        return a;
    };
    Query.processForUpdate = function (obj) {
        var o = {}, set, a = {};
        for (var id in obj) {
            if (id.startsWith('$'))
                a[id] = obj[id];
            else
                o[id] = obj[id];
        }
        a.obj = o;
        return a;
    };
    Query.createProjectionMap = function (proj) {
        return function (a) {
            Query.projection(a, proj.projection, proj.mode);
            return a;
        };
    };
    Query.projection = function (value, projection, mode, prefix) {
        if (prefix === undefined) {
            prefix = '';
        }
        var val, elim, l;
        for (var id in value) {
            val = value[id];
            elim = false;
            n = prefix + (prefix ? '.' : '') + id;
            if (mode == 0) {
                l = projection[n];
                if (l == 0) {
                    delete value[id];
                    elim = true;
                }
            } else if (mode == 1) {
                l = projection[n];
                if (n == '_id' && l == 0 || n != '_id' && l == undefined) {
                    delete value[id];
                    elim = true;
                }
            }
            if (!elim) {
                if (val instanceof Array) {
                    {
                        var o;
                        var $_iterator17 = regeneratorRuntime.values(val), $_normal17 = false, $_err17;
                        try {
                            while (true) {
                                var $_step17 = $_iterator17.next();
                                if ($_step17.done) {
                                    $_normal17 = true;
                                    break;
                                }
                                {
                                    o = $_step17.value;
                                    if (typeof o == 'object')
                                        Query.projection(o, projection, mode, n);
                                }
                            }
                        } catch (e) {
                            $_normal17 = false;
                            $_err17 = e;
                        }
                        try {
                            if (!$_normal17 && $_iterator17['return']) {
                                $_iterator17['return']();
                            }
                        } catch (e) {
                        }
                        if ($_err17) {
                            throw $_err17;
                        }
                    }
                } else if (typeof val == 'object') {
                    Query.projection(val, projection, mode, n);
                }
            }
        }
    };
    Query.compare = function (a, b) {
        val = true;
        var o, p;
        if (a instanceof Array && b) {
            p = {};
            for (var id in b) {
                p[id] = b[id];
            }
            delete p.$exists;
            b = p;
        }
        if (a instanceof Array && b instanceof Array) {
            if (b.length != a.length)
                return false;
            for (var i = 0; i < a.length; i++) {
                if (!this.compare(a[i], b[i]))
                    return false;
            }
            return true;
        }
        if (a instanceof Array) {
            {
                var c;
                var $_iterator14 = regeneratorRuntime.values(a), $_normal14 = false, $_err14;
                try {
                    while (true) {
                        var $_step14 = $_iterator14.next();
                        if ($_step14.done) {
                            $_normal14 = true;
                            break;
                        }
                        {
                            c = $_step14.value;
                            if (this.compare(c, b))
                                return true;
                        }
                    }
                } catch (e) {
                    $_normal14 = false;
                    $_err14 = e;
                }
                try {
                    if (!$_normal14 && $_iterator14['return']) {
                        $_iterator14['return']();
                    }
                } catch (e) {
                }
                if ($_err14) {
                    throw $_err14;
                }
            }
            return false;
        }
        if (b instanceof RegExp) {
            return b.test(a ? a.toString() : null);
        }
        if (typeof b == 'object') {
            for (var id in b) {
                if (id == '$lt')
                    val = a < b[id];
                else if (id == '$lte')
                    val = a <= b[id];
                else if (id == '$gt')
                    val = a > b[id];
                else if (id == '$gte')
                    val = a >= b[id];
                else if (id == '$in')
                    val = Query.compare(b[id], a);
                else if (id == '$nin')
                    val = !Query.compare(b[id], a);
                else if (id == '$ne')
                    val = a != b[id];
                else if (id == '$exists')
                    val = (a !== null && a !== undefined) == b[id];
                else if (id.startsWith('$'))
                    throw new core.System.Exception('No se pudo reconocer el operador especial ' + id);
                else {
                    o = b[id];
                    val = Query.compare(a ? a[id] : null, o);
                }
                if (!val)
                    break;
            }
        } else {
            if (typeof a == 'string' && typeof b == 'string')
                val = a.toUpperCase() == b.toUpperCase();
            else
                val = a == b;
        }
        return val;
    };
    Query.prototype.analize = function (conditions, index) {
        if (index === undefined) {
            index = 0;
        }
        var values = this.values, code = [], parts, str, y;
        code.push('a' + index + ': function(self){ ');
        if (this.$update || this.$remove)
            code.push('self=self.data;');
        for (var id in conditions) {
            val = conditions[id];
            if (val) {
                values.push(val);
                if (id == '$or') {
                    str = 'v=';
                    y = 0;
                    {
                        var query;
                        var $_iterator18 = regeneratorRuntime.values(val), $_normal18 = false, $_err18;
                        try {
                            while (true) {
                                var $_step18 = $_iterator18.next();
                                if ($_step18.done) {
                                    $_normal18 = true;
                                    break;
                                }
                                {
                                    query = $_step18.value;
                                    index++;
                                    this.analize(query, index + y + 1);
                                    str += (y > 0 ? '||' : '') + 'q.a' + (index + y + 1) + '(self)';
                                    y++;
                                }
                            }
                        } catch (e) {
                            $_normal18 = false;
                            $_err18 = e;
                        }
                        try {
                            if (!$_normal18 && $_iterator18['return']) {
                                $_iterator18['return']();
                            }
                        } catch (e) {
                        }
                        if ($_err18) {
                            throw $_err18;
                        }
                    }
                    code.push(str);
                    code.push('if(!v) return false');
                } else if (id == '$and') {
                    str = 'v=';
                    y = 0;
                    {
                        var query;
                        var $_iterator19 = regeneratorRuntime.values(val), $_normal19 = false, $_err19;
                        try {
                            while (true) {
                                var $_step19 = $_iterator19.next();
                                if ($_step19.done) {
                                    $_normal19 = true;
                                    break;
                                }
                                {
                                    query = $_step19.value;
                                    this.analize(query, index + y + 1);
                                    str += (y > 0 ? '&&' : '') + 'q.a' + (index + y + 1) + '(self)';
                                    y++;
                                }
                            }
                        } catch (e) {
                            $_normal19 = false;
                            $_err19 = e;
                        }
                        try {
                            if (!$_normal19 && $_iterator19['return']) {
                                $_iterator19['return']();
                            }
                        } catch (e) {
                        }
                        if ($_err19) {
                            throw $_err19;
                        }
                    }
                    code.push(str);
                    code.push('if(!v) return false');
                } else if (id == '$not') {
                    this.analize(val, index + 1);
                    code.push('v= !q.a' + (index + 1) + '(self)');
                    code.push('if(!v) return false');
                } else {
                    parts = id.split('.');
                    code.push('a= self');
                    if (parts.length == 1) {
                        for (var i = 0; i < parts.length; i++) {
                            code.push('a= a?a[' + JSON.stringify(parts[i]) + ']:null');
                        }
                        code.push('c=values[' + (values.length - 1) + ']');
                        code.push('v= Query.compare(a,c)');
                    } else {
                        code.push('c=values[' + (values.length - 1) + ']');
                        values.push(this.analizeParts(parts));
                        code.push('v= Query.gcompare(a, values[' + values.length - 1 + '], c)');
                    }
                    code.push('if(!v) return false');
                }
            }
        }
        code.push('return true');
        code.push('}');
        this.funcs.push(code);
    };
    Query.prototype.analizeSort = function (condition) {
        var code = [], parts, val;
        code.push('s0: function(y,z){');
        for (var id in condition) {
            parts = id.split('.');
            val = condition[id];
            for (var i = 0; i < parts.length; i++) {
                if (i == 0)
                    code.push('a= y?y.' + parts[i] + ':null');
                else
                    code.push('a= a?a.' + parts[i] + ':null');
            }
            for (var i = 0; i < parts.length; i++) {
                if (i == 0)
                    code.push('b= z?z.' + parts[i] + ':null');
                else
                    code.push('b= b?b.' + parts[i] + ':null');
            }
            code.push('if(a>b) return ' + (val > 0 ? 1 : -1));
            code.push('if(a<b) return ' + (val > 0 ? -1 : 1));
        }
        code.push('return 0');
        code.push('}');
        this.funcs.push(code);
    };
    Query.prototype.find = function (condition, projection) {
        if (condition && Object.keys(condition).length > 0) {
            this.analize(condition);
        }
        if (projection && Object.keys(projection).length > 0) {
            this.analizeProjection(projection);
        }
    };
    Query.prototype.update = function (condition, update, options) {
        this.$update = update;
        this.$updateOptions = options;
        if (condition && Object.keys(condition).length > 0) {
            this.analize(condition);
        }
    };
    Query.prototype.remove = function (condition, options) {
        this.$remove = true;
        this.$updateOptions = options;
        if (condition && Object.keys(condition).length > 0) {
            this.analize(condition);
        }
    };
    Query.prototype.analizeProjection = function (projection) {
        var val, mode, idmode;
        for (var id in projection) {
            val = projection[id];
            if (val != 0 && val != 1)
                throw new core.System.Exception('La proyección no es válida');
            if (mode != undefined && mode != val)
                throw new core.System.Exception('La proyección no es válida. No se puede usar modo eliminar y modo agregar de manera simultánea');
            if (id != '_id')
                mode = val | 0;
            else
                idmode = val | 0;
        }
        if (mode == undefined)
            mode = idmode;
        this.$projection = {
            mode: mode,
            projection: projection
        };
    };
    Query.prototype.sort = function (condition) {
        if (Object.keys(condition).length > 0) {
            this.analizeSort(condition);
            this.$sorting = true;
        }
    };
    Query.prototype.count = function () {
        return this.execute({ count: true });
    };
    Query.prototype.execute = function callee$0$0($_ref6) {
        var count, sto, update, remove, data, dataLength, oSkip, skip, l, analytics, i;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    count = $_ref6 ? $_ref6.count : undefined;
                    sto = this.table.$storage;
                    data = [], dataLength = 0;
                    oSkip = this.$skip | 0;
                    skip = this.$sorting ? 0 : oSkip;
                    update = this.$update;
                    remove = this.$remove;
                    analytics = !!update || !!remove;
                    if (update) {
                        update = Query.processForUpdate(update);
                        if (this.$updateOptions && !this.$updateOptions.multi)
                            this.$limit = 1;
                    }
                    if (this.funcs.length > 0) {
                        this.generateCode();
                    }
                    context$1$0.next = 12;
                    return regeneratorRuntime.awrap(sto.readAndSplit({
                        analytics: analytics,
                        ondata: function (self$0) {
                            return function (rows) {
                                if (self$0.q && self$0.q.a0)
                                    rows = rows.filter(self$0.q.a0);
                                var maxLength = rows.length;
                                if (!self$0.$sorting && self$0.$limit) {
                                    maxLength = Math.min(self$0.$limit - dataLength, maxLength);
                                }
                                if (count) {
                                    dataLength += maxLength;
                                } else {
                                    for (var i = skip; i < maxLength; i++) {
                                        data.push(rows[i]);
                                        dataLength++;
                                    }
                                    if (skip > 0)
                                        skip = Math.max(0, skip - maxLength);
                                }
                                if (self$0.$sorting) {
                                    if (maxLength > 0)
                                        data.sort(self$0.q.s0);
                                    if (self$0.$limit && data.length > self$0.$limit) {
                                        l = data.length - self$0.$limit;
                                        for (var i = 0; i < l; i++) {
                                            data.pop();
                                        }
                                    }
                                }
                                if (!self$0.$sorting && data.length > self$0.$limit) {
                                    return false;
                                }
                            };
                        }(this),
                        groupcount: this.$sorting ? 50 : this.$limit || 30
                    }));
                case 12:
                    if (!count) {
                        context$1$0.next = 14;
                        break;
                    }
                    return context$1$0.abrupt('return', dataLength);
                case 14:
                    if (oSkip > 0 && this.$sorting) {
                        for (i = 0; i < Math.min(data.length, oSkip); i++) {
                            data.shift();
                        }
                    }
                    if (!this.$update) {
                        context$1$0.next = 29;
                        break;
                    }
                    count = data.length;
                    if (!(data.length == 0 && this.$updateOptions && this.$updateOptions.upsert)) {
                        context$1$0.next = 23;
                        break;
                    }
                    data = Query.update({ _id: Kawasa.Util.uniqueId() }, update);
                    sto.insert(data);
                    count++;
                    context$1$0.next = 26;
                    break;
                case 23:
                    if (!(data.length > 0)) {
                        context$1$0.next = 26;
                        break;
                    }
                    context$1$0.next = 26;
                    return regeneratorRuntime.awrap(this._update(sto, data, update));
                case 26:
                    return context$1$0.abrupt('return', count);
                case 29:
                    if (!this.$remove) {
                        context$1$0.next = 37;
                        break;
                    }
                    count = data.length;
                    if (!(data.length > 0)) {
                        context$1$0.next = 34;
                        break;
                    }
                    context$1$0.next = 34;
                    return regeneratorRuntime.awrap(this._remove(sto, data));
                case 34:
                    return context$1$0.abrupt('return', count);
                case 37:
                    if (this.$projection) {
                        data = data.map(Query.createProjectionMap(this.$projection));
                    }
                case 38:
                    return context$1$0.abrupt('return', this.$one ? data[0] : data);
                case 39:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Query.prototype._update = function (sto, data, update) {
        var d;
        vw.log(update);
        for (var i = 0; i < data.length; i++) {
            d = data[i];
            d.data = Query.update(d.data, update);
        }
        return sto.insert(data, { edit: true });
    };
    Query.prototype._remove = function callee$0$0(sto, data) {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    return context$1$0.abrupt('return', sto.remove(data));
                case 1:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Query.prototype.limit = function (limit) {
        this.$limit = limit;
        return this;
    };
    Query.prototype.generateCode = function () {
        var str = this.createCode();
        var q;
        var values = this.values;
        eval(str);
        this.q = q;
    };
    Query.prototype.createCode = function () {
        var str = 'q={';
        var y = 0;
        {
            var code;
            var $_iterator13 = regeneratorRuntime.values(this.funcs), $_normal13 = false, $_err13;
            try {
                while (true) {
                    var $_step13 = $_iterator13.next();
                    if ($_step13.done) {
                        $_normal13 = true;
                        break;
                    }
                    {
                        code = $_step13.value;
                        str += code.join('\n');
                        if (y != this.funcs.length - 1)
                            str += ',';
                        y++;
                    }
                }
            } catch (e) {
                $_normal13 = false;
                $_err13 = e;
            }
            try {
                if (!$_normal13 && $_iterator13['return']) {
                    $_iterator13['return']();
                }
            } catch (e) {
            }
            if ($_err13) {
                throw $_err13;
            }
        }
        str += '}';
        return str;
    };
}
exports.default = Query;