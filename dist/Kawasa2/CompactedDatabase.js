var Kawasa = core.org.voxsoftware.Database.Kawasa;
var Fs = core.System.IO.Fs;
{
    var CompactedDatabase = function CompactedDatabase() {
        CompactedDatabase.$constructor ? CompactedDatabase.$constructor.apply(this, arguments) : CompactedDatabase.$superClass && CompactedDatabase.$superClass.apply(this, arguments);
    };
    CompactedDatabase.prototype = Object.create(Kawasa.Database.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(CompactedDatabase, Kawasa.Database) : CompactedDatabase.__proto__ = Kawasa.Database;
    CompactedDatabase.prototype.constructor = CompactedDatabase;
    CompactedDatabase.$super = Kawasa.Database.prototype;
    CompactedDatabase.$superClass = Kawasa.Database;
    CompactedDatabase.$constructor = function (file) {
        this.file = file;
        this.dStream = new Kawasa.StreamStorage.DirectoryStream(file);
    };
    CompactedDatabase.prototype.table = function (name) {
        return this.getTable(name);
        var table = new Kawasa.CompactedTable(name);
        table.db = this;
        table.start();
        return table;
    };
    CompactedDatabase.prototype.getTable = function callee$0$0(name) {
        var table;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    table = new Kawasa.CompactedTable(name);
                    table.db = this;
                    context$1$0.next = 4;
                    return regeneratorRuntime.awrap(table.start());
                case 4:
                    return context$1$0.abrupt('return', table);
                case 5:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
}
exports.default = CompactedDatabase;