var Stream = core.System.IO.Stream;
var emptyBlock = new Buffer(6 * 1024);
emptyBlock.fill(0);
var blockSize = 6 * 1024;
var Kawasa = core.org.voxsoftware.Database.Kawasa;
var debug = false;
{
    var DirectoryStream = function DirectoryStream() {
        DirectoryStream.$constructor ? DirectoryStream.$constructor.apply(this, arguments) : DirectoryStream.$superClass && DirectoryStream.$superClass.apply(this, arguments);
    };
    DirectoryStream.$constructor = function (stream) {
        if (!(stream instanceof Stream)) {
            this.$file = stream;
            stream = new core.System.IO.FileStream(stream, core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite);
        }
        this.$readBuffer = new Buffer(blockSize);
        this.$stream = stream;
        this.$breader = new core.System.IO.BinaryReader(stream);
        this.$bwriter = new core.System.IO.BinaryWriter(stream);
        this.start();
    };
    DirectoryStream.prototype.start = function () {
        var str;
        if (this.$stream.length == 0) {
            this.$bwriter.writeString('#KawDs');
        } else {
            str = this.$breader.readString();
            if (str != '#KawDs')
                throw new core.System.Exception('El formato no es válido');
        }
        this.$start = this.$stream.position;
    };
    DirectoryStream.prototype.getFileBlock = function callee$0$0($_ref7) {
        var getFirst, fromBlock, fromStart, block, nextBlock, b, first, pos, rr;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    getFirst = $_ref7 ? $_ref7.getFirst : undefined, fromBlock = $_ref7 ? $_ref7.fromBlock : undefined, fromStart = $_ref7 ? $_ref7.fromStart : undefined;
                    first = fromBlock;
                    if (!first) {
                        first = {
                            size: blockSize,
                            start: fromStart || this.$start
                        };
                    }
                    block = first;
                case 4:
                    if (!true) {
                        context$1$0.next = 28;
                        break;
                    }
                    rr = false;
                    pos = block.start + blockSize - 8;
                    if (this.virtualMode) {
                        if (this.$stream.length <= pos)
                            nextBlock = false;
                        else {
                            rr = true;
                        }
                    }
                    if (!rr) {
                        context$1$0.next = 13;
                        break;
                    }
                    this.$stream.position = pos;
                    context$1$0.next = 12;
                    return regeneratorRuntime.awrap(this.$breader.readBooleanAsync());
                case 12:
                    nextBlock = context$1$0.sent;
                case 13:
                    debug && vw.info('Hay otro bloque?', nextBlock);
                    if (!nextBlock) {
                        context$1$0.next = 25;
                        break;
                    }
                    b = block;
                    context$1$0.t0 = block;
                    context$1$0.t1 = blockSize;
                    context$1$0.next = 20;
                    return regeneratorRuntime.awrap(this.$breader.readUIntSafeAsync(true));
                case 20:
                    context$1$0.t2 = context$1$0.sent;
                    block = {
                        prev: context$1$0.t0,
                        size: context$1$0.t1,
                        start: context$1$0.t2
                    };
                    b.next = block;
                    context$1$0.next = 26;
                    break;
                case 25:
                    return context$1$0.abrupt('break', 28);
                case 26:
                    context$1$0.next = 4;
                    break;
                case 28:
                    if (getFirst) {
                        block = first;
                    }
                    return context$1$0.abrupt('return', block);
                case 30:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.getFileBlockSync = function ($_ref8) {
        var getFirst = $_ref8 ? $_ref8.getFirst : undefined, fromBlock = $_ref8 ? $_ref8.fromBlock : undefined, fromStart = $_ref8 ? $_ref8.fromStart : undefined;
        var block, nextBlock, b, first, rr, pos;
        first = fromBlock;
        if (!first) {
            first = {
                size: blockSize,
                start: fromStart || this.$start
            };
        }
        block = first;
        while (true) {
            rr = false;
            pos = block.start + blockSize - 8;
            if (this.virtualMode) {
                if (this.$stream.length <= pos)
                    nextBlock = false;
                else {
                    rr = true;
                }
            }
            if (rr) {
                this.$stream.position = pos;
                nextBlock = this.$breader.readBoolean();
            }
            debug && vw.info('Hay otro bloque?', nextBlock);
            if (nextBlock) {
                b = block;
                block = {
                    prev: block,
                    size: blockSize,
                    start: this.$breader.readUIntSafe(true)
                };
                b.next = block;
            } else {
                break;
            }
        }
        if (getFirst) {
            block = first;
        }
        return block;
    };
    DirectoryStream.prototype.getCurrentPosition = function callee$0$0(block) {
        var i, y;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$stream.position = block.start;
                    i = -1, y = 1;
                    context$1$0.next = 4;
                    return regeneratorRuntime.awrap(this.$stream.read(this.$readBuffer, 0, block.size - 8));
                case 4:
                    while ((i = this.$readBuffer.indexOf(10, i + 1)) >= 0) {
                        y = i + 1;
                    }
                    block.current = y;
                case 6:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.readFiles = function callee$0$0($_ref9) {
        var onfind, onlast, flen, find, buf, i, y, file2, tbuf, num1, num2, block, cblock, size1, size2;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    onfind = $_ref9 ? $_ref9.onfind : undefined, onlast = $_ref9 ? $_ref9.onlast : undefined;
                    flen = this.$stream.length;
                    if (!(flen < blockSize)) {
                        context$1$0.next = 6;
                        break;
                    }
                    this.$stream.position = this.$start;
                    context$1$0.next = 6;
                    return regeneratorRuntime.awrap(this.createFileBlock());
                case 6:
                    buf = new Buffer(2);
                    buf[0] = 10;
                    buf[1] = 200;
                    block = this.$firstFileBlock;
                    if (block) {
                        context$1$0.next = 16;
                        break;
                    }
                    context$1$0.next = 13;
                    return regeneratorRuntime.awrap(this.getFileBlock({ getFirst: true }));
                case 13:
                    block = this.$firstFileBlock = context$1$0.sent;
                    context$1$0.next = 18;
                    break;
                case 16:
                    context$1$0.next = 18;
                    return regeneratorRuntime.awrap(this.getFileBlock({ fromBlock: block }));
                case 18:
                    if (!block) {
                        context$1$0.next = 53;
                        break;
                    }
                    this.$stream.position = block.start;
                    cblock = block;
                    context$1$0.next = 23;
                    return regeneratorRuntime.awrap(this.$stream.read(this.$readBuffer, 0, blockSize));
                case 23:
                    readed = context$1$0.sent;
                    if (readed) {
                        context$1$0.next = 26;
                        break;
                    }
                    return context$1$0.abrupt('break', 53);
                case 26:
                    i = -1;
                case 27:
                    if (!((i = this.$readBuffer.indexOf(buf, i + 1)) >= 0)) {
                        context$1$0.next = 50;
                        break;
                    }
                    y = this.$readBuffer.indexOf(10, i + 2);
                    if (!(y >= 0)) {
                        context$1$0.next = 48;
                        break;
                    }
                    file2 = this.$readBuffer.toString('utf8', i + 2, y - 14);
                    if (!tbuf)
                        tbuf = new Buffer(8);
                    this.$readBuffer.copy(tbuf, 0, y - 7, y - 4);
                    this.$readBuffer.copy(tbuf, 4, y - 4, y);
                    tbuf[3] = 0;
                    num1 = tbuf.readUInt32LE(0);
                    num2 = tbuf.readUInt32LE(4);
                    size1 = num1 * 4294967295 + num2;
                    this.$readBuffer.copy(tbuf, 0, y - 14, y - 11);
                    this.$readBuffer.copy(tbuf, 4, y - 11, y - 7);
                    tbuf[3] = 0;
                    num1 = tbuf.readUInt32LE(0);
                    num2 = tbuf.readUInt32LE(4);
                    size2 = num1 * 4294967295 + num2;
                    find = {
                        blockstart: block.start,
                        initial: i,
                        final: y,
                        file: file2,
                        size: size2,
                        dataBlock: size1
                    };
                    if (!onfind) {
                        context$1$0.next = 48;
                        break;
                    }
                    if (!(onfind(find) === false)) {
                        context$1$0.next = 48;
                        break;
                    }
                    return context$1$0.abrupt('return', false);
                case 48:
                    context$1$0.next = 27;
                    break;
                case 50:
                    block = block.next;
                    context$1$0.next = 18;
                    break;
                case 53:
                    if (onlast && cblock) {
                        onlast(cblock);
                    }
                case 54:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.readAllFiles = function callee$0$0() {
        var data;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    data = [];
                    context$1$0.next = 3;
                    return regeneratorRuntime.awrap(this.readFiles({
                        onfind: function (find) {
                            data.push({
                                file: find.file,
                                size: find.size
                            });
                        }
                    }));
                case 3:
                    return context$1$0.abrupt('return', data);
                case 4:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.open = function callee$0$0(file, mode, access) {
        var b, block, str, create, datablock, f;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    create = true;
                    if (mode === undefined)
                        mode = core.System.IO.FileMode.Open;
                    if ((mode & core.System.IO.FileMode.Create) != core.System.IO.FileMode.Create) {
                        if ((mode & core.System.IO.FileMode.OpenOrCreate) != core.System.IO.FileMode.OpenOrCreate)
                            create = false;
                    }
                    context$1$0.next = 5;
                    return regeneratorRuntime.awrap(this.readFiles({
                        onfind: function (find) {
                            if (find.file == file) {
                                b = find;
                                return false;
                            }
                        },
                        onlast: function (l) {
                            block = l;
                        }
                    }));
                case 5:
                    if (b) {
                        context$1$0.next = 34;
                        break;
                    }
                    if (create) {
                        context$1$0.next = 10;
                        break;
                    }
                    throw new core.System.IO.FileNotFoundException(file);
                case 10:
                    context$1$0.next = 12;
                    return regeneratorRuntime.awrap(this.getCurrentPosition(block));
                case 12:
                    str = new Buffer(file);
                    if (!(1 + str.length + 1 + 8 > block.size - block.current)) {
                        context$1$0.next = 18;
                        break;
                    }
                    context$1$0.next = 16;
                    return regeneratorRuntime.awrap(this.createFileBlock(block));
                case 16:
                    block = context$1$0.sent;
                    block.current = 1;
                case 18:
                    context$1$0.next = 20;
                    return regeneratorRuntime.awrap(this.createDataBlock());
                case 20:
                    datablock = context$1$0.sent;
                    this.$stream.position = block.current + block.start;
                    b = {
                        initial: block.current,
                        file: file,
                        blockstart: block.start,
                        dataBlock: datablock.start
                    };
                    this.$bwriter.writeByte(200);
                    context$1$0.next = 26;
                    return regeneratorRuntime.awrap(this.$stream.writeAsync(str, 0, str.length));
                case 26:
                    context$1$0.next = 28;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUIntSafeAsync(0, true));
                case 28:
                    context$1$0.next = 30;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUIntSafeAsync(datablock.start, true));
                case 30:
                    this.$bwriter.writeByte(10);
                    b.final = this.$stream.position - 1 - block.start;
                case 32:
                    context$1$0.next = 37;
                    break;
                case 34:
                    context$1$0.next = 36;
                    return regeneratorRuntime.awrap(this.getFileBlock({
                        getFirst: true,
                        fromStart: b.dataBlock
                    }));
                case 36:
                    datablock = context$1$0.sent;
                case 37:
                    f = new Kawasa.StreamStorage.InFileStream(this, b, datablock);
                    if (!((mode & core.System.IO.FileMode.Truncate) == core.System.IO.FileMode.Truncate)) {
                        context$1$0.next = 41;
                        break;
                    }
                    context$1$0.next = 41;
                    return regeneratorRuntime.awrap(f.setLengthAsync(0));
                case 41:
                    if (access === undefined)
                        access = core.System.IO.FileAccess.Read;
                    if ((access & core.System.IO.FileAccess.Read) != core.System.IO.FileAccess.Read)
                        f.$canread = false;
                    if ((access & core.System.IO.FileAccess.Write) != core.System.IO.FileAccess.Write) {
                        f.$canwrite = false;
                    }
                    return context$1$0.abrupt('return', f);
                case 45:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.createFileBlock = function callee$0$0(prev) {
        var block;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    context$1$0.next = 2;
                    return regeneratorRuntime.awrap(this.createDataBlock(prev));
                case 2:
                    block = context$1$0.sent;
                    this.$stream.position = block.start;
                    this.$bwriter.writeByte(10);
                    block.current = 1;
                    return context$1$0.abrupt('return', block);
                case 7:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.createDataBlock = function callee$0$0(prev) {
        var len, block;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    len = this.$stream.position = this.virtualMode ? this.$length : this.$stream.length;
                    if (this.virtualMode) {
                        context$1$0.next = 7;
                        break;
                    }
                    emptyBlock[0] = 0;
                    context$1$0.next = 5;
                    return regeneratorRuntime.awrap(this.$stream.writeAsync(emptyBlock, 0, emptyBlock.length));
                case 5:
                    context$1$0.next = 8;
                    break;
                case 7:
                    this.$length += emptyBlock.length;
                case 8:
                    block = {
                        size: blockSize,
                        start: len
                    };
                    if (!prev) {
                        context$1$0.next = 18;
                        break;
                    }
                    if (!this.virtualMode) {
                        context$1$0.next = 14;
                        break;
                    }
                    this.$virtual.push({
                        prev: prev,
                        block: block
                    });
                    context$1$0.next = 16;
                    break;
                case 14:
                    context$1$0.next = 16;
                    return regeneratorRuntime.awrap(this.writeStart(prev, block));
                case 16:
                    prev.next = block;
                    block.prev = prev;
                case 18:
                    return context$1$0.abrupt('return', block);
                case 19:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.createDataBlockSync = function (prev) {
        var len = this.$stream.position = this.virtualMode ? this.$length : this.$stream.length;
        if (!this.virtualMode) {
            emptyBlock[0] = 0;
            this.$stream.write(emptyBlock, 0, emptyBlock.length);
        } else {
            this.$length += emptyBlock.length;
        }
        var block = {
            size: blockSize,
            start: len
        };
        if (prev) {
            if (this.virtualMode) {
                this.$virtual.push({
                    prev: prev,
                    block: block
                });
            } else {
                this.writeStart(prev, block);
            }
            prev.next = block;
            block.prev = prev;
        }
        return block;
    };
    DirectoryStream.prototype.writeStart = function callee$0$0(prev, block) {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$stream.position = prev.start + prev.size - 8;
                    this.$bwriter.writeByte(1);
                    debug && vw.warning('Writing uintsafe', block.start);
                    context$1$0.next = 5;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUIntSafeAsync(block.start, true));
                case 5:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    DirectoryStream.prototype.writeStartSync = function (prev, block) {
        this.$stream.position = prev.start + prev.size - 8;
        this.$bwriter.writeByte(1);
        debug && vw.warning('Writing uintsafe', block.start);
        this.$bwriter.writeUIntSafe(block.start, true);
    };
    DirectoryStream.prototype.enableVirtualMode = function () {
        this.$length = this.$stream.length;
        this.virtualMode = true;
        this.$virtual = [];
    };
    DirectoryStream.prototype.disableVirtualMode = function callee$0$0() {
        var pos, dif, wr, v, $_iterator20, $_normal20, $_err20, $_step20;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    pos = this.$stream.position;
                    this.$stream.position = this.$stream.length;
                    if (!(this.$length !== undefined)) {
                        context$1$0.next = 28;
                        break;
                    }
                    dif = this.$length - this.$stream.length;
                    if (dif > 0) {
                        this.$stream.setLength(this.$length);
                    }
                    $_iterator20 = regeneratorRuntime.values(this.$virtual), $_normal20 = false;
                    context$1$0.prev = 6;
                case 7:
                    if (!true) {
                        context$1$0.next = 17;
                        break;
                    }
                    $_step20 = $_iterator20.next();
                    if (!$_step20.done) {
                        context$1$0.next = 12;
                        break;
                    }
                    $_normal20 = true;
                    return context$1$0.abrupt('break', 17);
                case 12:
                    v = $_step20.value;
                    context$1$0.next = 15;
                    return regeneratorRuntime.awrap(this.writeStart(v.prev, v.block));
                case 15:
                    context$1$0.next = 7;
                    break;
                case 17:
                    context$1$0.next = 23;
                    break;
                case 19:
                    context$1$0.prev = 19;
                    context$1$0.t0 = context$1$0['catch'](6);
                    $_normal20 = false;
                    $_err20 = context$1$0.t0;
                case 23:
                    try {
                        if (!$_normal20 && $_iterator20['return']) {
                            $_iterator20['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err20) {
                        context$1$0.next = 26;
                        break;
                    }
                    throw $_err20;
                case 26:
                    this.$virtual = undefined;
                    this.$stream.position = pos;
                case 28:
                    this.$length = undefined;
                    this.virtualMode = false;
                case 30:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this, [[
                6,
                19
            ]]);
    };
    DirectoryStream.prototype.disableVirtualModeSync = function () {
        var pos = this.$stream.position;
        this.$stream.position = this.$stream.length;
        if (this.$length !== undefined) {
            var dif = this.$length - this.length, wr;
            if (dif > 0) {
                this.$stream.setLength(this.$length);
            }
            {
                var v;
                var $_iterator21 = regeneratorRuntime.values(this.$virtual), $_normal21 = false, $_err21;
                try {
                    while (true) {
                        var $_step21 = $_iterator21.next();
                        if ($_step21.done) {
                            $_normal21 = true;
                            break;
                        }
                        {
                            v = $_step21.value;
                            this.writeStartSync(v.prev, v.block);
                        }
                    }
                } catch (e) {
                    $_normal21 = false;
                    $_err21 = e;
                }
                try {
                    if (!$_normal21 && $_iterator21['return']) {
                        $_iterator21['return']();
                    }
                } catch (e) {
                }
                if ($_err21) {
                    throw $_err21;
                }
            }
            this.$virtual = undefined;
            this.$stream.position = pos;
        }
        this.$length = undefined;
        this.virtualMode = false;
    };
}
exports.default = DirectoryStream;