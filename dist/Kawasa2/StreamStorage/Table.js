var $mod$6 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var Kawasa = core.org.voxsoftware.Database.Kawasa;
var space = new Buffer(50);
space.fill(0);
{
    var Table = function Table() {
        Table.$constructor ? Table.$constructor.apply(this, arguments) : Table.$superClass && Table.$superClass.apply(this, arguments);
    };
    Table.$constructor = function (stream, stream2, table) {
        this.$stream = stream;
        this.$stream2 = stream2;
        this.$pending = [];
        this.$table = table;
        this.createDocument();
        this.readBuffer = new Buffer(9 * 1024);
    };
    Table.prototype.queue = function (action) {
        action.task = new core.VW.Task();
        this.$pending.push(action);
        if (!this.$working)
            this.doQueue();
        return action.task;
    };
    Table.prototype.doQueue = function callee$0$0() {
        var pending, er, action, $_iterator22, $_normal22, $_err22, $_step22;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$working = true;
                    pending = this.$pending;
                    this.$pending = [];
                    context$1$0.prev = 3;
                    $_iterator22 = regeneratorRuntime.values(pending), $_normal22 = false;
                    context$1$0.prev = 5;
                case 6:
                    if (!true) {
                        context$1$0.next = 24;
                        break;
                    }
                    $_step22 = $_iterator22.next();
                    if (!$_step22.done) {
                        context$1$0.next = 11;
                        break;
                    }
                    $_normal22 = true;
                    return context$1$0.abrupt('break', 24);
                case 11:
                    action = $_step22.value;
                    context$1$0.prev = 12;
                    context$1$0.next = 15;
                    return regeneratorRuntime.awrap(this.realize(action));
                case 15:
                    action.task.result = context$1$0.sent;
                    context$1$0.next = 21;
                    break;
                case 18:
                    context$1$0.prev = 18;
                    context$1$0.t0 = context$1$0['catch'](12);
                    action.task.exception = context$1$0.t0;
                case 21:
                    action.task.finish();
                    context$1$0.next = 6;
                    break;
                case 24:
                    context$1$0.next = 30;
                    break;
                case 26:
                    context$1$0.prev = 26;
                    context$1$0.t1 = context$1$0['catch'](5);
                    $_normal22 = false;
                    $_err22 = context$1$0.t1;
                case 30:
                    try {
                        if (!$_normal22 && $_iterator22['return']) {
                            $_iterator22['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err22) {
                        context$1$0.next = 33;
                        break;
                    }
                    throw $_err22;
                case 33:
                    context$1$0.next = 38;
                    break;
                case 35:
                    context$1$0.prev = 35;
                    context$1$0.t2 = context$1$0['catch'](3);
                    er = context$1$0.t2;
                case 38:
                    if (!(!er && this.$pending.length)) {
                        context$1$0.next = 41;
                        break;
                    }
                    this.doQueue();
                    return context$1$0.abrupt('return');
                case 41:
                    this.$working = false;
                    if (!er) {
                        context$1$0.next = 44;
                        break;
                    }
                    throw er;
                case 44:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this, [
            [
                3,
                35
            ],
            [
                5,
                26
            ],
            [
                12,
                18
            ]
        ]);
    };
    Table.prototype.waitQueue = function () {
        var tasks = this.$pending.map(function (x) {
            return x.task;
        });
        return core.VW.Task.waitMany(tasks);
    };
    Table.prototype.realize = function (action) {
        if (action.type == 'insert') {
            return this.insert(action.docs);
        } else if (action.type == 'remove') {
            action.query.remove(action.args[0], action.args[1]);
            return action.query.execute();
        } else if (action.type == 'update') {
            action.query.update(action.args[0], action.args[1], action.args[2]);
            return action.query.execute();
        } else if (action.type == 'countall') {
            return this.count();
        }
    };
    Table.prototype.createDocument = function () {
        this.$breader = new core.System.IO.BinaryReader(this.$stream);
        this.$bwriter = new core.System.IO.BinaryWriter(this.$stream);
        this.$breader2 = new core.System.IO.BinaryReader(this.$stream2);
        this.$bwriter2 = new core.System.IO.BinaryWriter(this.$stream2);
        var str;
        this.$stream.position = 0;
        vw.log(this.$stream.length);
        if (this.$stream.length == 0) {
            this.$bwriter.writeString('kaw#');
            this.$start = this.$stream.position;
            this.$bwriter.writeUInt32(0);
            this.$bwriter.writeByte(10);
        } else {
            str = this.$breader.readString();
            if (str != 'kaw#')
                throw new core.System.Exception('El archivo de tabla tiene un formato no válido');
            this.$start = this.$stream.position;
        }
        this.$stream.position = this.$stream.length;
    };
    Table.prototype._añadirConteo = function callee$0$0(count) {
        var c;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$stream.position = this.$start;
                    context$1$0.next = 3;
                    return regeneratorRuntime.awrap(this.$breader.readUInt32Async());
                case 3:
                    context$1$0.t0 = context$1$0.sent;
                    context$1$0.t1 = count || 1;
                    c = context$1$0.t0 + context$1$0.t1;
                    this.$stream.position = this.$start;
                    context$1$0.next = 9;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUInt32Async(c));
                case 9:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.findAll = function callee$0$0() {
        var data;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    data = [];
                    context$1$0.next = 3;
                    return regeneratorRuntime.awrap(this.readAndSplit({
                        ondata: function (rows) {
                            for (var i = 0; i < rows.length; i++) {
                                data.push(rows[i]);
                            }
                        },
                        groupcount: 10000
                    }));
                case 3:
                    return context$1$0.abrupt('return', data);
                case 4:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.remove = function callee$0$0(positions) {
        var count, position, $_iterator23, $_normal23, $_err23, $_step23;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    count = 0;
                    $_iterator23 = regeneratorRuntime.values(positions), $_normal23 = false;
                    context$1$0.prev = 2;
                case 3:
                    if (!true) {
                        context$1$0.next = 15;
                        break;
                    }
                    $_step23 = $_iterator23.next();
                    if (!$_step23.done) {
                        context$1$0.next = 8;
                        break;
                    }
                    $_normal23 = true;
                    return context$1$0.abrupt('break', 15);
                case 8:
                    position = $_step23.value;
                    this.$stream.position = position.idxstart + 1;
                    context$1$0.next = 12;
                    return regeneratorRuntime.awrap(this.$stream.writeByteAsync(199));
                case 12:
                    count--;
                    context$1$0.next = 3;
                    break;
                case 15:
                    context$1$0.next = 21;
                    break;
                case 17:
                    context$1$0.prev = 17;
                    context$1$0.t0 = context$1$0['catch'](2);
                    $_normal23 = false;
                    $_err23 = context$1$0.t0;
                case 21:
                    try {
                        if (!$_normal23 && $_iterator23['return']) {
                            $_iterator23['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err23) {
                        context$1$0.next = 24;
                        break;
                    }
                    throw $_err23;
                case 24:
                    context$1$0.next = 26;
                    return regeneratorRuntime.awrap(this._añadirConteo(count));
                case 26:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this, [[
                2,
                17
            ]]);
    };
    Table.prototype.readUIntSafe = function (tbuf) {
        var num1, num2;
        var res = tbuf[7];
        tbuf[7] = tbuf[6];
        tbuf[6] = tbuf[5];
        tbuf[5] = tbuf[4];
        tbuf[4] = tbuf[3];
        tbuf[3] = 0;
        num1 = tbuf.readUInt32LE(0);
        num2 = tbuf.readUInt32LE(4);
        tbuf[3] = tbuf[4];
        tbuf[4] = tbuf[5];
        tbuf[5] = tbuf[6];
        tbuf[6] = tbuf[7];
        tbuf[7] = res;
        return num1 * 4294967295 + num2;
    };
    Table.prototype.readAndSplit = function callee$0$0($_ref10) {
        var ondata, groupcount, analytics, noData, search, lead, endSearch, str, d, str2, pos1, pos2, buf, data, pos, bufr;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    ondata = $_ref10 ? $_ref10.ondata : undefined, groupcount = $_ref10 ? $_ref10.groupcount : undefined, analytics = $_ref10 ? $_ref10.analytics : undefined, noData = $_ref10 ? $_ref10.noData : undefined;
                    search = -1;
                    buf = new Buffer(2), data = [];
                    buf[0] = 10;
                    buf[1] = 200;
                    this.$stream.position = this.$start;
                case 6:
                    if (!true) {
                        context$1$0.next = 43;
                        break;
                    }
                    pos = this.$stream.position;
                    context$1$0.next = 10;
                    return regeneratorRuntime.awrap(this.readPortion());
                case 10:
                    if (!context$1$0.sent) {
                        context$1$0.next = 40;
                        break;
                    }
                    search = -1;
                    if (lead)
                        this.currentBuffer = Buffer.concat([
                            lead,
                            this.currentBuffer
                        ]);
                case 13:
                    if (!((search = this.currentBuffer.indexOf(buf, search + 1)) >= 0)) {
                        context$1$0.next = 34;
                        break;
                    }
                    endSearch = search + 16;
                    if (!(endSearch > 0)) {
                        context$1$0.next = 31;
                        break;
                    }
                    str = this.currentBuffer.slice(search + 2, search + 10);
                    str2 = this.currentBuffer.slice(search + 9, search + 17);
                    pos1 = this.readUIntSafe(str);
                    pos2 = this.readUIntSafe(str2);
                    d = null;
                    if (noData) {
                        context$1$0.next = 28;
                        break;
                    }
                    if (!bufr || bufr.length < pos2 - pos1) {
                        bufr = new Buffer(pos2 - pos1);
                        bufr.fill(0);
                    }
                    this.$stream2.position = pos1;
                    context$1$0.next = 26;
                    return regeneratorRuntime.awrap(this.$stream2.readAsync(bufr, 0, pos2 - pos1));
                case 26:
                    d = bufr.toString('utf8', 0, pos2 - pos1);
                    d = JSON.parse(d);
                case 28:
                    if (analytics) {
                        data.push({
                            data: d,
                            portionstart: pos,
                            idxstart: pos + search,
                            idxend: endSearch + pos,
                            start: pos1,
                            end: pos2
                        });
                    } else {
                        if (!noData)
                            data.push(d);
                    }
                    context$1$0.next = 32;
                    break;
                case 31:
                    lead = new Buffer(this.currentBuffer.slice(search, this.currentBuffer.length));
                case 32:
                    context$1$0.next = 13;
                    break;
                case 34:
                    if (!(data.length >= groupcount)) {
                        context$1$0.next = 38;
                        break;
                    }
                    if (ondata(data)) {
                        context$1$0.next = 37;
                        break;
                    }
                    return context$1$0.abrupt('break', 43);
                case 37:
                    data = [];
                case 38:
                    context$1$0.next = 41;
                    break;
                case 40:
                    return context$1$0.abrupt('break', 43);
                case 41:
                    context$1$0.next = 6;
                    break;
                case 43:
                    if (data.length > 0) {
                        ondata(data);
                    }
                case 44:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.readPortion = function callee$0$0() {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    context$1$0.next = 2;
                    return regeneratorRuntime.awrap(this.$stream.readAsync(this.readBuffer, 0, this.readBuffer.length));
                case 2:
                    this.readed = context$1$0.sent;
                    this.currentBuffer = this.readBuffer.slice(0, this.readed);
                    return context$1$0.abrupt('return', this.readed > 0);
                case 5:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Table.prototype.insert = function callee$0$0(document, $_ref11) {
        var edit, documents, positions, er, buffer, len, id, json, _id, slen, doc, after, $_iterator24, $_normal24, $_err24, $_step24;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    edit = $_ref11 ? $_ref11.edit : undefined;
                    positions = [];
                    if (document instanceof Array) {
                        documents = document;
                    } else {
                        documents = [document];
                    }
                    slen = this.$stream.length;
                    this.$stream.position = slen;
                    this.$stream2.position = this.$stream2.length;
                    context$1$0.prev = 6;
                    after = 2;
                    $_iterator24 = regeneratorRuntime.values(documents), $_normal24 = false;
                    context$1$0.prev = 9;
                case 10:
                    if (!true) {
                        context$1$0.next = 45;
                        break;
                    }
                    $_step24 = $_iterator24.next();
                    if (!$_step24.done) {
                        context$1$0.next = 15;
                        break;
                    }
                    $_normal24 = true;
                    return context$1$0.abrupt('break', 45);
                case 15:
                    document = $_step24.value;
                    if (edit) {
                        doc = document;
                        document = document.data;
                    }
                    if (!document._id || !edit) {
                        id = Kawasa.Util.uniqueId();
                        document._id = id;
                    }
                    json = core.safeJSON.stringify(document);
                    json += '\n';
                    len = Buffer.byteLength(json);
                    if (!buffer || buffer.length < len) {
                        buffer = new Buffer(len);
                    }
                    buffer.write(json);
                    if (edit) {
                        if (len <= doc.end - doc.start) {
                            this.$stream2.position = doc.start;
                        } else {
                            doc.start = this.$stream2.position = this.$stream2.length;
                        }
                    } else {
                        doc = { start: this.$stream2.position };
                    }
                    context$1$0.next = 26;
                    return regeneratorRuntime.awrap(this.$bwriter2.writeBytesAsync(buffer, len));
                case 26:
                    if (!edit) {
                        context$1$0.next = 34;
                        break;
                    }
                    this.$stream.position = doc.idxstart + 2;
                    context$1$0.next = 30;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUIntSafeAsync(doc.start, true));
                case 30:
                    context$1$0.next = 32;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUIntSafeAsync(doc.start + len, true));
                case 32:
                    context$1$0.next = 43;
                    break;
                case 34:
                    vw.log('Inserting;', doc.start, len);
                    context$1$0.next = 37;
                    return regeneratorRuntime.awrap(this.$bwriter.writeByteAsync(200));
                case 37:
                    context$1$0.next = 39;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUIntSafeAsync(doc.start, true));
                case 39:
                    context$1$0.next = 41;
                    return regeneratorRuntime.awrap(this.$bwriter.writeUIntSafeAsync(doc.start + len, true));
                case 41:
                    context$1$0.next = 43;
                    return regeneratorRuntime.awrap(this.$bwriter.writeByteAsync(10));
                case 43:
                    context$1$0.next = 10;
                    break;
                case 45:
                    context$1$0.next = 51;
                    break;
                case 47:
                    context$1$0.prev = 47;
                    context$1$0.t0 = context$1$0['catch'](9);
                    $_normal24 = false;
                    $_err24 = context$1$0.t0;
                case 51:
                    try {
                        if (!$_normal24 && $_iterator24['return']) {
                            $_iterator24['return']();
                        }
                    } catch (e) {
                    }
                    if (!$_err24) {
                        context$1$0.next = 54;
                        break;
                    }
                    throw $_err24;
                case 54:
                    context$1$0.next = 65;
                    break;
                case 56:
                    context$1$0.prev = 56;
                    context$1$0.t1 = context$1$0['catch'](6);
                    if (!this.$stream.setLengthAsync) {
                        context$1$0.next = 63;
                        break;
                    }
                    context$1$0.next = 61;
                    return regeneratorRuntime.awrap(this.$stream.setLengthAsync(slen));
                case 61:
                    context$1$0.next = 64;
                    break;
                case 63:
                    this.$stream.setLength(slen);
                case 64:
                    er = context$1$0.t1;
                case 65:
                    if (!er) {
                        context$1$0.next = 67;
                        break;
                    }
                    throw er;
                case 67:
                    if (edit) {
                        context$1$0.next = 70;
                        break;
                    }
                    context$1$0.next = 70;
                    return regeneratorRuntime.awrap(this._añadirConteo(documents.length));
                case 70:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this, [
            [
                6,
                56
            ],
            [
                9,
                47
            ]
        ]);
    };
    Table.prototype.count = function callee$0$0() {
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this.$stream.position = this.$start;
                    context$1$0.next = 3;
                    return regeneratorRuntime.awrap(this.$breader.readUInt32());
                case 3:
                    return context$1$0.abrupt('return', context$1$0.sent);
                case 4:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
}
exports.default = Table;