var Kawasa = core.org.voxsoftware.Database.Kawasa;
{
    var Sesion = function Sesion() {
        Sesion.$constructor ? Sesion.$constructor.apply(this, arguments) : Sesion.$superClass && Sesion.$superClass.apply(this, arguments);
    };
    Sesion.$constructor = function (options) {
        this.options = options;
    };
    Sesion.prototype.handle = function callee$0$0(args) {
        var cookie, c, time, id;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    cookie = args.request.cookie;
                    if (!cookie)
                        cookie = new core.VW.Http.ServerCookie();
                    id = cookie.get('kawasa_id');
                    if (!id) {
                        context$1$0.next = 8;
                        break;
                    }
                    context$1$0.next = 6;
                    return regeneratorRuntime.awrap(this.options.collection.findOne({ _id: id }).execute());
                case 6:
                    c = context$1$0.sent;
                    if (c && c.maxAge !== undefined && c.maxAge > 0) {
                        time = parseInt(Date.now() - c.accesed);
                        if (time > c.maxAge) {
                            id = undefined;
                        }
                    }
                case 8:
                    if (!c)
                        id = undefined;
                    if (id) {
                        context$1$0.next = 16;
                        break;
                    }
                    c = {
                        data: {},
                        updated: Date.now(),
                        created: Date.now(),
                        accesed: Date.now(),
                        maxAge: this.options.maxAge || 0
                    };
                    context$1$0.next = 13;
                    return regeneratorRuntime.awrap(this.options.collection.insert(c));
                case 13:
                    cookie.set('kawasa_id', c._id);
                    context$1$0.next = 18;
                    break;
                case 16:
                    context$1$0.next = 18;
                    return regeneratorRuntime.awrap(this.options.collection.update({ _id: id }, { $set: { accesed: Date.now() } }));
                case 18:
                    if (!id)
                        args.response.setHeader('set-cookie', cookie.toStr('kawasa_id'));
                    c.data = c.data || {};
                    args.request.session = c.data;
                    args.request.cookie = cookie;
                    if (!args.continue) {
                        context$1$0.next = 26;
                        break;
                    }
                    context$1$0.next = 25;
                    return regeneratorRuntime.awrap(args.continue());
                case 25:
                    return context$1$0.abrupt('return', context$1$0.sent);
                case 26:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Sesion.prototype.save = function callee$0$0(args) {
        var cookie, id;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    cookie = args.request.cookie;
                    if (cookie) {
                        context$1$0.next = 3;
                        break;
                    }
                    return context$1$0.abrupt('return');
                case 3:
                    id = cookie.get('kawasa_id');
                    context$1$0.next = 6;
                    return regeneratorRuntime.awrap(this.options.collection.update({ _id: id }, {
                        $set: {
                            accesed: Date.now(),
                            data: args.request.session,
                            updated: Date.now()
                        }
                    }));
                case 6:
                    if (!args.continue) {
                        context$1$0.next = 10;
                        break;
                    }
                    context$1$0.next = 9;
                    return regeneratorRuntime.awrap(args.continue());
                case 9:
                    return context$1$0.abrupt('return', context$1$0.sent);
                case 10:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
    Sesion.prototype.destroy = function callee$0$0(args) {
        var cookie, id;
        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
            while (1)
                switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    cookie = args.request.cookie;
                    id = cookie.get('kawasa_id');
                    if (id) {
                        context$1$0.next = 4;
                        break;
                    }
                    return context$1$0.abrupt('return');
                case 4:
                    context$1$0.next = 6;
                    return regeneratorRuntime.awrap(this.options.collection.remove({ _id: id }));
                case 6:
                    if (!args.continue) {
                        context$1$0.next = 10;
                        break;
                    }
                    context$1$0.next = 9;
                    return regeneratorRuntime.awrap(args.continue());
                case 9:
                    return context$1$0.abrupt('return', context$1$0.sent);
                case 10:
                case 'end':
                    return context$1$0.stop();
                }
        }, null, this);
    };
}
exports.default = Sesion;