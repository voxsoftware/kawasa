var Kawasa= require("../main")
import Path from 'path'
Kawasa.Table && Kawasa.Database && Kawasa.StreamStorage.Table
var dir= Path.join(__dirname, "..", "db")


class TestInsert{

  async initDB(){
    var time= Date.now()
    var st= new core.System.IO.FileStream(Path.join(dir, "Disk.kw"), core.System.IO.FileMode.OpenOrCreate,core.System.IO.FileAccess.ReadWrite
      )
    this.db= new Kawasa.StreamStorage.DirectoryStream(st)

    this.file= await this.db.open("archivo1.db", core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite)
    vw.log("Iniciando Kawasa:", Date.now()-time,"ms")
  }


  async initFile2(){
    var time= Date.now()
    this.file2= await this.db.open("archivo2.db", core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite)
    vw.log("Abriendo archivo 2:", Date.now()-time,"ms")
  }

  async writeText(){
    b= new Buffer("James es el mejor")
    vw.info("File length 1:", this.file.length)
    this.file.position= this.file.length
    await this.file.writeAsync(b, 0, b.length)
  }

  async readText(){
    b= new Buffer(40)
    //vw.info("File length:", this.file.length)
    this.file.position= 0
    var readed= await this.file.readAsync(b, 0, b.length)
    vw.log("Leído: ", b.toString('utf8', 0,readed))
  }

  async writeText2(){
    b= new Buffer("Todo el mundo sabe como un borracho se ace")
    vw.info("File length 2:", this.file2.length)
    this.file2.position= this.file2.length
    await this.file2.writeAsync(b, 0, b.length)
  }

  async writeText3(){
    b= new Buffer(8000)
    for(var i=0;i<1000;i++){
      b.write("a123456\n", i*8, 8)
    }
    vw.info("File length 3:", this.file2.length)
    this.file2.position= this.file2.length
    await this.file2.writeAsync(b, 0, b.length)
  }

  async readText2(){
    b= new Buffer(100)
    //vw.info("File length:", this.file.length)
    this.file2.position= 0
    var readed= await this.file2.readAsync(b, 0, b.length)
    vw.log("Leído: ", b.toString('utf8', 0,readed))
  }

  async readText3(){
    b= new Buffer(100)
    //vw.info("File length:", this.file.length)
    this.file2.position= this.file2.length-3500
    var readed= await this.file2.readAsync(b, 0, b.length)
    vw.log("Leído: ", b.toString('utf8', 0,readed))
  }


  async readAllFiles(){
    var files= await this.db.readAllFiles()
    vw.log("Files: ", files)
  }

  async main(){
    try{
      await this.initDB()
      await this.readAllFiles()
      await this.writeText()
      await this.readText()

      await this.initFile2()
      //await this.writeText2()
      await this.writeText3()
      //await this.readText2()
      await this.readText3()

      await this.readAllFiles()

    }
    catch(e){
      vw.error("Error al ejecutar el test: ",e)
    }
  }

}


(new TestInsert()).main()
