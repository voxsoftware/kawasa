import Path from 'path'
var Kawasa= require("../main")
var Datastore = require('nedb')
var db, table, db2
var init= ()=>{

  var dir= Path.join(__dirname, "..", "db")

  try{
    Kawasa.Database&&Kawasa.Table
    var time= Date.now()
    db= new Kawasa.Database(dir)
    table= db.table("usuarios")
    vw.log("Iniciando Kawasa:", Date.now()-time)
  }
  catch(e){
    vw.error(e)
  }
}

var test1= async()=>{
  try{
    var time
    time= Date.now()
    for(var i=0;i<1000;i++){
      await table.insert({
        "nombres": "Michael James",
        "apellidos": "Suárez Rivera",
        i
      })
    }
    vw.info(Date.now()-time, "ms")

  }
  catch(e){
    vw.error(e)
  }
}


var funcAsAsync= function(func, self, args){
  var task= new core.VW.Task()
  args= args||[]
  args.push(function(err,data){
    if(err)
      task.exception= err

    task.result= data
    task.finish()
  })
  func.apply(self, args)
  return task
}


var initDB2=async ()=>{
  var time= Date.now()
  db2 = new Datastore({ filename: Path.join(__dirname, "..", "db", "usuarios2.db") });
  try{
    await funcAsAsync(db2.loadDatabase, db2, [])
    vw.log("INiciando nedb: ", Date.now()-time)

    await funcAsAsync(db2.ensureIndex, db2, [{fieldName: "i"}])
  }
  catch(e){
    vw.error(e)
  }
}
var test3= async ()=>{




  try{

    time= Date.now()
    for(var i=0;i<1000;i++){
      await funcAsAsync(db2.insert, db2, [{
        "nombres": "Michael James",
        "apellidos": "Suárez Rivera",
        i
      }])
    }
    vw.info(Date.now()-time, "ms")

  }
  catch(e){
    vw.error(e)
  }
}





var test2= async()=>{
  try{
    var time
    time= Date.now()
    var count= await table.count()
    vw.info(Date.now()-time, "ms", "Cantidad:", count)

  }
  catch(e){
    vw.error(e)
  }
}

var test4= async()=>{
  try{
    var time
    time= Date.now()
    var count= await funcAsAsync(db2.count, db2, [{}])
    vw.info(Date.now()-time, "ms", "Cantidad:", count)

    time= Date.now()
    var count= await funcAsAsync(db2.count, db2, [{i:10}])
    vw.info(Date.now()-time, "ms", "Cantidad:", count)

  }
  catch(e){
    vw.error(e)
  }
}

var test5= async()=>{
  try{
    var time
    time= Date.now()
    var count= await table.readId("192596qhjrtoqmiyics67z")
    vw.info(Date.now()-time, "ms", "Obj:", count)

  }
  catch(e){
    vw.error(e)
  }
}


var test7= async()=>{
  try{
    var time
    time= Date.now()
    var count= await table.findAll()
    vw.info(Date.now()-time, "ms", "Obj:", count.length)

  }
  catch(e){
    vw.error(e)
  }
}


var test6= async()=>{
  try{
    var time
    time= Date.now()
    var count= await funcAsAsync(db2.find, db2, [{
      _id:"1b67onpHNiYYbiZR"
    }])
    vw.info(Date.now()-time, "ms", "Obj:", count)

  }
  catch(e){
    vw.error(e)
  }
}





var execute= async()=>{
  init()
  await initDB2()
  //await test1()
  //await test2()


  //await test3()
  //await test1()
  await test5()
  await test7()

/*
  await test2()
  await test4()
*/
}

execute()
