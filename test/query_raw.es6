var Kawasa= require("../main")
import Path from 'path'
Kawasa.Table && Kawasa.Database && Kawasa.StreamStorage.Table
var dir= Path.join(__dirname, "..", "db")


class TestQuery{

  /*initDB(){
    var time= Date.now()
    this.db= new Kawasa.Database(dir)
    this.table= this.db.table("tabla1")
    vw.log("Iniciando Kawasa:", Date.now()-time,"ms")
  }*/

  initQuery(){
    this.query= new Kawasa.Query()
  }

  async query1(){
    var time= Date.now()
    this.query.analize({
      planet:"Earth",
      satellites: {$in:["Moon"]},
      $or:[
        {
          num: 1
        },{
          num: 2
        }
      ]
    })
    this.query.generateCode()
    vw.log("Query 1: ", Date.now()-time, "ms")
    vw.warning(this.query.q)
  }


  async main(){
    try{
      this.initQuery()
      //await this.insertByOne()
      //await this.insertMultiple()
      await this.query1()
    }
    catch(e){
      vw.error("Error al ejecutar el test: ",e)
    }
  }



  



}


(new TestQuery()).main()
