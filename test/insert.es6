var Kawasa= require("../main")
import Path from 'path'
Kawasa.Table && Kawasa.Database && Kawasa.StreamStorage.Table
var dir= Path.join(__dirname, "..", "db")


class TestInsert{

  initDB(){
    var time= Date.now()
    this.db= new Kawasa.Database(dir)
    this.table= this.db.table("tabla1")
    vw.log("Iniciando Kawasa:", Date.now()-time,"ms")
  }

  async insertByOne(){
    var time= Date.now()
    for(var i=0;i<1000;i++){
      await this.table.insert({
        "nombres": "John",
        "apellidos": "Dohe",
        "num": i
      })
    }
    vw.log("Inserting by one: ", Date.now()-time, "ms")
  }



  async insertWaitingQueue(){
    var time= Date.now()
    for(var i=0;i<1000;i++){
      this.table.insert({
        "nombres": "John",
        "apellidos": "Dohe",
        "num": i
      })
    }
    await this.table.waitQueue()
    vw.log("Inserting waiting queue: ", Date.now()-time, "ms")
  }

  async insertMultiple(){
    var time= Date.now(),obs=[]
    for(var i=0;i<1000;i++){
      obs.push({
        "nombres": "John",
        "apellidos": "Dohe",
        "num": i
      })
    }
    await this.table.insert(obs)
    vw.log("Inserting multiple: ", Date.now()-time, "ms")
  }



  async main(){
    try{
      this.initDB()
      //await this.insertByOne()
      //await this.insertMultiple()
      await this.insertWaitingQueue()
    }
    catch(e){
      vw.error("Error al ejecutar el test: ",e)
    }
  }

}


(new TestInsert()).main()
