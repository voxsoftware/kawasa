var Kawasa= require("../main")
import Path from 'path'
Kawasa.Table && Kawasa.CompactedDatabase && Kawasa.CompactedTable && Kawasa.StreamStorage.Table && Kawasa.Query
var dir= Path.join(__dirname, "..", "db")


class TestInsert{

  async initDB(){
    var time= Date.now()
    this.db= new Kawasa.CompactedDatabase(Path.join(dir, "test1.kwdb"))
    this.table= await this.db.getTable("tabla1")
    vw.log("Iniciando Kawasa:", Date.now()-time,"ms")
  }

  async initDBModo2(){
    var time= Date.now()
    this.db= new Kawasa.CompactedDatabase(Path.join(dir, "test2.kwdb"))
    this.table= await this.db.getTable("tabla1")
    vw.log("Iniciando Kawasa:", Date.now()-time,"ms")
  }

  async insertByOne(){
    var time= Date.now()
    for(var i=0;i<1000;i++){
      await this.table.insert({
        "nombres": "John",
        "apellidos": "Dohe",
        "num": i
      })
    }
    vw.log("Inserting by one: ", Date.now()-time, "ms")
  }



  async insertWaitingQueue(){
    var time= Date.now()
    for(var i=0;i<1000;i++){
      this.table.insert({
        "nombres": "John",
        "apellidos": "Dohe",
        "num": i
      })
    }
    await this.table.waitQueue()
    vw.log("Inserting waiting queue: ", Date.now()-time, "ms")
  }

  async insertMultiple(){
    var time= Date.now(),obs=[]
    for(var i=0;i<1000;i++){
      obs.push({
        "nombres": "John",
        "apellidos": "Dohe",
        "num": i
      })
    }
    await this.table.insert(obs)
    vw.log("Inserting multiple: ", Date.now()-time, "ms")


  }

  async findAll(){
    var time= Date.now()
    var obs= await this.table.find({}).limit(4000).execute()
    vw.log("Obteniendo datos: ", Date.now()-time, "ms count:", obs.length)

  }


  async main(){
    try{
      //await  this.initDBModo()
      await  this.initDBModo2()

      //await this.insertByOne()
      await this.insertMultiple()
      await this.insertMultiple()
      await this.findAll()
      //await this.insertWaitingQueue()
    }
    catch(e){
      vw.error("Error al ejecutar el test: ",e)
    }
  }

}


(new TestInsert()).main()
