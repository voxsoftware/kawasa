var Kawasa= require("../main")
import Path from 'path'
Kawasa.Table && Kawasa.Database && Kawasa.StreamStorage.Table && Kawasa.Query
var dir= Path.join(__dirname, "..", "db")


class TestInsert{

  initDB(){
    var time= Date.now()
    this.db= new Kawasa.Database(dir)
    this.table= this.db.table("planetas")
    vw.log("Iniciando Kawasa:", Date.now()-time,"ms")
  }



  async insertData(){


    // Contar sin filtros ...
    var count= await this.table.count()
    if(count>0)
      return

    var time= Date.now()
    var data= [

      {
        num: 1,
        nombre: "Mercurio",
        habitado: false,
        tamaño: 1
      },
      {
        num: 2,
        nombre: "Venus",
        habitado: false,
        tamaño: 4
      },
      {
        num: 3,
        nombre: "Tierra",
        habitado: true,
        tamaño: 3,
        humanos:{
          genero:2,
          felices: 0
        },
        satelites: [
          "luna"
        ]
      },
      {
        num: 4,
        nombre: "Marte",
        habitado: 2, // Maybe ... jaja
        tamaño: 2.8,
        color: "rojo"
      },
      {
        num: 5,
        nombre: "Júpiter",
        habitado: false,
        tamaño: 10
      },
      {
        num: 6,
        nombre: "Saturno",
        habitado: false,
        tamaño: 9,
        anillos: true,
        satelites:[
          "satelite1",
          "satelite2",
          "satelite3",
          "satelite4",
          "satelite5"
        ]
      }
    ]
    await this.table.insert(data)
    vw.log("Insertando datos: ", Date.now()-time, "ms")

  }


  async conSatelites(){
    var time= Date.now()
    var q= this.table.find({
      satelites: {
        $exists: true
      }
    })

    var data=await q.execute()
    vw.log("Obteniendo información: ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async conSatelite(satelite){
    var time= Date.now()
    var q= this.table.find({
      satelites: satelite
    })

    var data=await q.execute()
    vw.log("Obteniendo información (satelite luna): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async habitado(){
    var time= Date.now()
    var q= this.table.find({
      habitado:{
        $in:[2,true]
      }
    })

    var data=await q.execute()
    vw.log("Obteniendo información (habitado): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async habitadoModoOrCustom(){
    var time= Date.now()
    var q= this.table.find({
      $or: [
        {
          habitado: 2
        },
        {
          habitado: true
        }
      ],
      satelites: {
        $exists: true
      }
    })
    /*vw.warning(q.q.a0.toString())
    vw.warning(q.q.a0.toString())
    vw.warning(q.q.a0.toString())
    */
    var data=await q.execute()
    vw.log("Obteniendo información (habitado modo or): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async habitadoModoOr(){
    var time= Date.now()
    var q= this.table.find({
      $or: [
        {
          habitado: 2
        },
        {
          habitado: true
        }
      ]
    })
    /*vw.warning(q.q.a0.toString())
    vw.warning(q.q.a0.toString())
    vw.warning(q.q.a0.toString())
    */
    var data=await q.execute()
    vw.log("Obteniendo información (habitado modo or): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async leerTodo(){
    var time= Date.now()
    var q= this.table.find({

    })
    var data=await q.execute()
    vw.log("Obteniendo información (leer todo): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async leerTodoSort(){
    var time= Date.now()
    var q= this.table.find({

    })
    q.sort({
      num:-1
    })
    var data=await q.execute()
    vw.log("Obteniendo información (leer todo sorting): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async leerTodoSort2(){
    var time= Date.now()
    var q= this.table.find({

    })
    q.sort({
      tamaño: 1
    })
    var data=await q.execute()
    vw.log("Obteniendo información (leer todo sorting tamaño): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async leerTodoSortLimit(limit){
    var time= Date.now()
    var q= this.table.find({

    })
    q.sort({
      num:-1
    })
    q.limit(limit)
    var data=await q.execute()
    vw.log("Obteniendo información (leer todo sorting and limit): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async leerTodoLimit(limit){
    var time= Date.now()
    var q= this.table.find({

    })
    q.limit(limit)
    var data=await q.execute()
    vw.log("Obteniendo información (leer todo): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async leerProjection(){
    var time= Date.now()
    var q= this.table.find({
    },{
      nombre:1,
      habitado: 1,
      tamaño:1
    })

    var data=await q.execute()
    vw.log("Obteniendo información (leer projection): ", Date.now()-time, "ms")
    vw.warning(data)
  }

  async leerProjection2(){
    var time= Date.now()
    var q= this.table.find({
    },{
      tamaño:0,
      habitado: 0,
      "humanos.genero": 0,
      num:0
    })

    var data=await q.execute()
    vw.log("Obteniendo información (leer projection): ", Date.now()-time, "ms")
    vw.warning(data)
  }


  async main(){
    try{
      this.initDB()
      await this.insertData()
      await this.conSatelites()
      await this.conSatelite("luna")
      await this.habitado()
      await this.habitadoModoOr()
      await this.habitadoModoOrCustom()
      await this.leerTodo()
      await this.leerTodoLimit(2)
      await this.leerTodoSort()
      await this.leerTodoSortLimit(2)
      await this.leerTodoSort2()

      await this.leerProjection()
      await this.leerProjection2()
      //await this.insertByOne()
      //await this.insertMultiple()
      //await this.insertWaitingQueue()
    }
    catch(e){
      vw.error("Error al ejecutar el test: ",e)
    }
  }

}


(new TestInsert()).main()
