var Kawasa= require("../main")
import Path from 'path'
Kawasa.Table && Kawasa.Database && Kawasa.StreamStorage.Table && Kawasa.Query
var dir= Path.join(__dirname, "..", "db")


class TestInsert{

  initDB(){
    var time= Date.now()
    this.db= new Kawasa.Database(dir)
    this.table= this.db.table("empleados")
    vw.log("Iniciando Kawasa:", Date.now()-time,"ms")
  }



  async insertData(){

    var count= await this.table.count()
    if(count>0)
      return

    var time= Date.now()
    var data= [

      {
        num: 1,
        nombre: "Michael James",
        apellido: "Suárez Rivera",
        sueldo: 1000
      },
      {
        num: 2,
        nombre: "Francisco",
        apellido: "Barros",
        sueldo: 1100
      },
      {
        num: 3,
        nombre: "Naila Anaís",
        apellido: "Cifuentes Gutiérrez",
        sueldo: 1500,
        familiar: [
          {
            nombre: "Enrique",
            apellido:"Cifuentes",
            parentesco: "Padre"
          }
        ]
      }

    ]
    await this.table.insert(data)
    vw.log("Insertando datos: ", Date.now()-time, "ms")

  }



  async update(){
    var time= Date.now()
    await this.table.update({
      num: 1
    },{
      $inc:{
        sueldo: 100
      }
    })
    vw.log("Actualizando datos: ", Date.now()-time, "ms")

  }

  async remove(){
    var time= Date.now()
    await this.table.remove({
      num: 2
    })
    vw.log("Actualizando datos: ", Date.now()-time, "ms")
    var count= await this.table.count()
    vw.log("Tamaño después de borrar:", count)

  }


  async main(){
    try{
      this.initDB()
      await this.insertData()
      await this.update()
      await this.remove()
    }
    catch(e){
      vw.error("Error al ejecutar el test: ",e)
    }
  }

}


(new TestInsert()).main()
